﻿/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2010 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * Chinese Traditional language file.
 */

var FCKLang =
{
// Language direction : "ltr" (left to right) or "rtl" (right to left).
Dir					: "ltr",

ToolbarCollapse		: "折叠工具栏",
ToolbarExpand		: "展开工具栏",

// Toolbar Items and Context Menu
Save				: "保存",
NewPage				: "新建",
Preview				: "预览",
Cut					: "剪切",
Copy				: "复制",
Paste				: "粘贴",
PasteText			: "粘贴为纯文字格式",
PasteWord			: "自 Word 粘贴",
Print				: "打印",
SelectAll			: "全选",
RemoveFormat		: "清除格式",
InsertLinkLbl		: "超链接",
InsertLink			: "插入/编辑超链接",
RemoveLink			: "移除超链接",
VisitLink			: "开启超链接",
Anchor				: "插入/编辑锚点",
AnchorDelete		: "移除锚点",
InsertImageLbl		: "图像",
InsertImage			: "插入/编辑图像",
InsertFlashLbl		: "Flash",
InsertFlash			: "插入/编辑 Flash",
InsertTableLbl		: "表格",
InsertTable			: "插入/编辑表格",
InsertLineLbl		: "水平线",
InsertLine			: "插入水平线",
InsertSpecialCharLbl: "特殊符号",
InsertSpecialChar	: "插入特殊符号",
InsertSmileyLbl		: "表情符号",
InsertSmiley		: "插入表情符号",
About				: "关于 FCKeditor",
Bold				: "粗体",
Italic				: "斜体",
Underline			: "底线",
StrikeThrough		: "删除线",
Subscript			: "下标",
Superscript			: "上标",
LeftJustify			: "左对齐",
CenterJustify		: "居中对齐",
RightJustify		: "右对齐",
BlockJustify		: "两端对齐",
DecreaseIndent		: "减少缩进量",
IncreaseIndent		: "增加缩进量",
Blockquote			: "引用文字",
CreateDiv			: "插入 Div 标签",
EditDiv				: "编辑 Div 标签",
DeleteDiv			: "删除 Div 标签",
Undo				: "撤消",
Redo				: "重做",
NumberedListLbl		: "编号清单",
NumberedList		: "插入/移除编号清单",
BulletedListLbl		: "项目清单",
BulletedList		: "插入/移除项目清单",
ShowTableBorders	: "显示表格边框",
ShowDetails			: "显示详细数据",
Style				: "样式",
FontFormat			: "格式",
Font				: "字体",
FontSize			: "大小",
TextColor			: "文字颜色",
BGColor				: "背景颜色",
Source				: "源代码",
Find				: "寻找",
Replace				: "取代",
SpellCheck			: "拼写检查",
UniversalKeyboard	: "通用软键盘",
PageBreakLbl		: "分页符号",
PageBreak			: "插入分页符号",

Form			: "表单",
Checkbox		: "复选框",
RadioButton		: "单选按钮",
TextField		: "单行文本",
Textarea		: "文字区域",
HiddenField		: "隐藏栏位",
Button			: "按钮",
SelectionField	: "清单/选单",
ImageButton		: "图像按钮",

FitWindow		: "全屏编辑",
ShowBlocks		: "显示区块",

// Context Menu
EditLink			: "编辑超链接",
CellCM				: "单元格",
RowCM				: "行",
ColumnCM			: "列",
InsertRowAfter		: "向下插入行",
InsertRowBefore		: "向上插入行",
DeleteRows			: "删除行",
InsertColumnAfter	: "向右插入列",
InsertColumnBefore	: "向左插入列",
DeleteColumns		: "删除列",
InsertCellAfter		: "向右插入储存格",
InsertCellBefore	: "向左插入储存格",
DeleteCells			: "删除储存格",
MergeCells			: "合并储存格",
MergeRight			: "向右合并单元格",
MergeDown			: "向下合并单元格",
HorizontalSplitCell	: "水平拆分单元格",
VerticalSplitCell	: "垂直拆分单元格",
TableDelete			: "删除表格",
CellProperties		: "单元格属性",
TableProperties		: "表格属性",
ImageProperties		: "图像属性",
FlashProperties		: "Flash 属性",

AnchorProp			: "锚点属性",
ButtonProp			: "按钮属性",
CheckboxProp		: "复选框属性",
HiddenFieldProp		: "隐藏栏位属性",
RadioButtonProp		: "单选按钮属性",
ImageButtonProp		: "图像按钮属性",
TextFieldProp		: "单行文本属性",
SelectionFieldProp	: "菜单/列表属性",
TextareaProp		: "多行文本属性",
FormProp			: "表单属性",

FontFormats			: "一般;已格式化;地址;标题 1;标题 2;标题 3;标题 4;标题 5;标题 6;一般 (DIV)",

// Alerts and Messages
ProcessingXHTML		: "处理 XHTML 中，请稍候…",
Done				: "完成",
PasteWordConfirm	: "您要粘贴的内容好像是来自 MS Word，是否要清除 MS Word 格式后再粘贴？",
NotCompatiblePaste	: "该命令需要 Internet Explorer 5.5 或更高版本的支持，是否按常规粘贴进行？",
UnknownToolbarItem	: "未知工具栏项目 \"%1\"",
UnknownCommand		: "未知命令名称 \"%1\"",
NotImplemented		: "命令无法执行",
UnknownToolbarSet	: "工具栏设定 \"%1\" 不存在",
NoActiveX			: "浏览器的安全性设定限制了本编辑器的某些功能。您必须启用安全性设定中的“执行ActiveX控制项与插件”项目，否则本编辑器将会出现错误并缺少某些功能",
BrowseServerBlocked : "无法打开资源浏览器，请确认是否启用了禁止弹出窗口。",
DialogBlocked		: "无法打开对话框窗口，请确认是否启用了禁止弹出窗口或网页对话框（IE）。",
VisitLinkBlocked	: "无法打开新窗口，请确认是否启用了禁止弹出窗口或网页对话框（IE）。",

// Dialogs
DlgBtnOK			: "确定",
DlgBtnCancel		: "取消",
DlgBtnClose			: "关闭",
DlgBtnBrowseServer	: "浏览服务器端",
DlgAdvancedTag		: "高级",
DlgOpOther			: "<其他>",
DlgInfoTab			: "信息",
DlgAlertUrl			: "请插入 URL",

// General Dialogs Labels
DlgGenNotSet		: "<尚未设定>",
DlgGenId			: "ID",
DlgGenLangDir		: "语言方向",
DlgGenLangDirLtr	: "由左而右 (LTR)",
DlgGenLangDirRtl	: "由右而左 (RTL)",
DlgGenLangCode		: "语言代码",
DlgGenAccessKey		: "访问键",
DlgGenName			: "名称",
DlgGenTabIndex		: "Tab 键次序",
DlgGenLongDescr		: "详细 URL",
DlgGenClass			: "样式表类别",
DlgGenTitle			: "标题",
DlgGenContType		: "内容类型",
DlgGenLinkCharset	: "连结资源之编码",
DlgGenStyle			: "样式",

// Image Dialog
DlgImgTitle			: "图像属性",
DlgImgInfoTab		: "图像信息",
DlgImgBtnUpload		: "上传至服务器",
DlgImgURL			: "URL",
DlgImgUpload		: "上传",
DlgImgAlt			: "替代文字",
DlgImgWidth			: "宽度",
DlgImgHeight		: "高度",
DlgImgLockRatio		: "等比例",
DlgBtnResetSize		: "重设为原大小",
DlgImgBorder		: "边框",
DlgImgHSpace		: "水平距离",
DlgImgVSpace		: "垂直距离",
DlgImgAlign			: "对齐",
DlgImgAlignLeft		: "靠左对齐",
DlgImgAlignAbsBottom: "绝对下方",
DlgImgAlignAbsMiddle: "绝对中间",
DlgImgAlignBaseline	: "基准线",
DlgImgAlignBottom	: "靠下对齐",
DlgImgAlignMiddle	: "置中对齐",
DlgImgAlignRight	: "靠右对齐",
DlgImgAlignTextTop	: "文字上方",
DlgImgAlignTop		: "靠上对齐",
DlgImgPreview		: "预览",
DlgImgAlertUrl		: "请输入图像 URL",
DlgImgLinkTab		: "超链接",

// Flash Dialog
DlgFlashTitle		: "Flash 属性",
DlgFlashChkPlay		: "自动播放",
DlgFlashChkLoop		: "重复",
DlgFlashChkMenu		: "开启Flash菜单",
DlgFlashScale		: "缩放",
DlgFlashScaleAll	: "全部显示",
DlgFlashScaleNoBorder	: "无边框",
DlgFlashScaleFit	: "精确符合",

// Link Dialog
DlgLnkWindowTitle	: "超链接",
DlgLnkInfoTab		: "超链接信息",
DlgLnkTargetTab		: "目标",

DlgLnkType			: "超连接类型",
DlgLnkTypeURL		: "URL",
DlgLnkTypeAnchor	: "页内锚点链接",
DlgLnkTypeEMail		: "电子邮件",
DlgLnkProto			: "通讯协议",
DlgLnkProtoOther	: "<其他>",
DlgLnkURL			: "URL",
DlgLnkAnchorSel		: "请选择锚点",
DlgLnkAnchorByName	: "按锚点名称",
DlgLnkAnchorById	: "按锚点 ID",
DlgLnkNoAnchors		: "(此文档没有可用的锚点)",
DlgLnkEMail			: "电子邮件",
DlgLnkEMailSubject	: "邮件主旨",
DlgLnkEMailBody		: "邮件内容",
DlgLnkUpload		: "上传",
DlgLnkBtnUpload		: "发送到服务器上",

DlgLnkTarget		: "目标",
DlgLnkTargetFrame	: "<框架>",
DlgLnkTargetPopup	: "<弹出窗口>",
DlgLnkTargetBlank	: "新窗口 (_blank)",
DlgLnkTargetParent	: "父窗口 (_parent)",
DlgLnkTargetSelf	: "本窗口 (_self)",
DlgLnkTargetTop		: "最上层窗口 (_top)",
DlgLnkTargetFrameName	: "目标框架名称",
DlgLnkPopWinName	: "弹出窗口名称",
DlgLnkPopWinFeat	: "弹出窗口属性",
DlgLnkPopResize		: "可调整大小",
DlgLnkPopLocation	: "网址栏",
DlgLnkPopMenu		: "菜单栏",
DlgLnkPopScroll		: "滚动条",
DlgLnkPopStatus		: "状态栏",
DlgLnkPopToolbar	: "工具栏",
DlgLnkPopFullScrn	: "全屏幕 (IE)",
DlgLnkPopDependent	: "从属 (NS)",
DlgLnkPopWidth		: "宽",
DlgLnkPopHeight		: "高",
DlgLnkPopLeft		: "左",
DlgLnkPopTop		: "右",

DlnLnkMsgNoUrl		: "请输入欲连结的 URL",
DlnLnkMsgNoEMail	: "请输入电子邮件位址",
DlnLnkMsgNoAnchor	: "请选择锚点",
DlnLnkMsgInvPopName	: "弹出名称必须以“英文字母”为开头，且不得含有空白",

// Color Dialog
DlgColorTitle		: "请选择颜色",
DlgColorBtnClear	: "清除",
DlgColorHighlight	: "预览",
DlgColorSelected	: "选择",

// Smiley Dialog
DlgSmileyTitle		: "插入表情符号",

// Special Character Dialog
DlgSpecialCharTitle	: "请选择特殊符号",

// Table Dialog
DlgTableTitle		: "表格属性",
DlgTableRows		: "行数",
DlgTableColumns		: "列数",
DlgTableBorder		: "边框",
DlgTableAlign		: "对齐",
DlgTableAlignNotSet	: "<未设定>",
DlgTableAlignLeft	: "靠左对齐",
DlgTableAlignCenter	: "置中",
DlgTableAlignRight	: "靠右对齐",
DlgTableWidth		: "宽度",
DlgTableWidthPx		: "像素",
DlgTableWidthPc		: "百分比",
DlgTableHeight		: "高度",
DlgTableCellSpace	: "间距",
DlgTableCellPad		: "内距",
DlgTableCaption		: "标题",
DlgTableSummary		: "摘要",
DlgTableHeaders		: "标题单元格",	//MISSING
DlgTableHeadersNone		: "无",	//MISSING
DlgTableHeadersColumn	: "第一列",	//MISSING
DlgTableHeadersRow		: "第一行",	//MISSING
DlgTableHeadersBoth		: "第一列和第一行",	//MISSING

// Table Cell Dialog
DlgCellTitle		: "储存格属性",
DlgCellWidth		: "宽度",
DlgCellWidthPx		: "像素",
DlgCellWidthPc		: "百分比",
DlgCellHeight		: "高度",
DlgCellWordWrap		: "自动换行",
DlgCellWordWrapNotSet	: "<尚未设定>",
DlgCellWordWrapYes	: "是",
DlgCellWordWrapNo	: "否",
DlgCellHorAlign		: "水平对齐",
DlgCellHorAlignNotSet	: "<尚未设定>",
DlgCellHorAlignLeft	: "靠左对齐",
DlgCellHorAlignCenter	: "置中",
DlgCellHorAlignRight: "靠右对齐",
DlgCellVerAlign		: "垂直对齐",
DlgCellVerAlignNotSet	: "<尚未设定>",
DlgCellVerAlignTop	: "靠上对齐",
DlgCellVerAlignMiddle	: "置中",
DlgCellVerAlignBottom	: "靠下对齐",
DlgCellVerAlignBaseline	: "基准线",
DlgCellType		: "储存格类型",
DlgCellTypeData		: "数据",
DlgCellTypeHeader	: "标题",
DlgCellRowSpan		: "合并行数",
DlgCellCollSpan		: "合并列数",
DlgCellBackColor	: "背景颜色",
DlgCellBorderColor	: "边框颜色",
DlgCellBtnSelect	: "请选择…",

// Find and Replace Dialog
DlgFindAndReplaceTitle	: "查找和替换",

// Find Dialog
DlgFindTitle		: "查找",
DlgFindFindBtn		: "查找",
DlgFindNotFoundMsg	: "未找到指定的文字。",

// Replace Dialog
DlgReplaceTitle			: "替换",
DlgReplaceFindLbl		: "查找:",
DlgReplaceReplaceLbl	: "替换:",
DlgReplaceCaseChk		: "区分大小写",
DlgReplaceReplaceBtn	: "替换",
DlgReplaceReplAllBtn	: "全部替换",
DlgReplaceWordChk		: "全字匹配",

// Paste Operations / Dialog
PasteErrorCut	: "浏览器的安全性设定不允许编辑器自动执行剪下动作。请使用快捷键 (Ctrl+X) 剪下。",
PasteErrorCopy	: "浏览器的安全性设定不允许编辑器自动执行复制动作。请使用快捷键 (Ctrl+C) 复制。",

PasteAsText		: "粘贴为无格式文本",
PasteFromWord	: "从 MS Word 粘贴",

DlgPasteMsg2	: "请使用快捷键 (<strong>Ctrl+V</strong>) 贴到下方区域中并按下 <strong>确定</strong>",
DlgPasteSec		: "因为浏览器的安全性设定，本编辑器无法直接存取您的剪贴簿数据，请您自行在本窗口进行贴上动作。",
DlgPasteIgnoreFont		: "忽略字型设定",
DlgPasteRemoveStyles	: "清理样式设定",

// Color Picker
ColorAutomatic	: "自动",
ColorMoreColors	: "更多颜色…",

// Document Properties
DocProps		: "页面属性",

// Anchor Dialog
DlgAnchorTitle		: "命名锚点",
DlgAnchorName		: "锚点名称",
DlgAnchorErrorName	: "请输入锚点名称",

// Speller Pages Dialog
DlgSpellNotInDic		: "不在字典中",
DlgSpellChangeTo		: "更改为",
DlgSpellBtnIgnore		: "忽略",
DlgSpellBtnIgnoreAll	: "全部忽略",
DlgSpellBtnReplace		: "替换",
DlgSpellBtnReplaceAll	: "全部替换",
DlgSpellBtnUndo			: "撤消",
DlgSpellNoSuggestions	: "- 无建议值 -",
DlgSpellProgress		: "正在进行拼写检查...",
DlgSpellNoMispell		: "拼写检查完成：未发现拼写错误",
DlgSpellNoChanges		: "拼写检查完成：未更改任何单字",
DlgSpellOneChange		: "拼写检查完成：更改了 1 个单字",
DlgSpellManyChanges		: "拼写检查完成：更改了 %1 个单字",

IeSpellDownload			: "尚未安装拼写检查插件。您是否想要现在下载？",

// Button Dialog
DlgButtonText		: "显示文字 (值)",
DlgButtonType		: "类型",
DlgButtonTypeBtn	: "按钮 (Button)",
DlgButtonTypeSbm	: "提交 (Submit)",
DlgButtonTypeRst	: "重设 (Reset)",

// Checkbox and Radio Button Dialogs
DlgCheckboxName		: "名称",
DlgCheckboxValue	: "选取值",
DlgCheckboxSelected	: "已选取",

// Form Dialog
DlgFormName		: "名称",
DlgFormAction	: "动作",
DlgFormMethod	: "方法",

// Select Field Dialog
DlgSelectName		: "名称",
DlgSelectValue		: "选取值",
DlgSelectSize		: "大小",
DlgSelectLines		: "行",
DlgSelectChkMulti	: "可多选",
DlgSelectOpAvail	: "可用选项",
DlgSelectOpText		: "显示文字",
DlgSelectOpValue	: "值",
DlgSelectBtnAdd		: "新增",
DlgSelectBtnModify	: "修改",
DlgSelectBtnUp		: "上移",
DlgSelectBtnDown	: "下移",
DlgSelectBtnSetValue : "设为预设值",
DlgSelectBtnDelete	: "删除",

// Textarea Dialog
DlgTextareaName	: "名称",
DlgTextareaCols	: "字符宽度",
DlgTextareaRows	: "行数",

// Text Field Dialog
DlgTextName			: "名称",
DlgTextValue		: "值",
DlgTextCharWidth	: "字符宽度",
DlgTextMaxChars		: "最多字符数",
DlgTextType			: "类型",
DlgTextTypeText		: "文本",
DlgTextTypePass		: "密码",

// Hidden Field Dialog
DlgHiddenName	: "名称",
DlgHiddenValue	: "值",

// Bulleted List Dialog
BulletedListProp	: "项目清单属性",
NumberedListProp	: "编号清单属性",
DlgLstStart			: "起始编号",
DlgLstType			: "清单类型",
DlgLstTypeCircle	: "圆圈",
DlgLstTypeDisc		: "圆点",
DlgLstTypeSquare	: "方块",
DlgLstTypeNumbers	: "数字 (1, 2, 3)",
DlgLstTypeLCase		: "小写字母 (a, b, c)",
DlgLstTypeUCase		: "大写字母 (A, B, C)",
DlgLstTypeSRoman	: "小写罗马数字 (i, ii, iii)",
DlgLstTypeLRoman	: "大写罗马数字 (I, II, III)",

// Document Properties Dialog
DlgDocGeneralTab	: "一般",
DlgDocBackTab		: "背景",
DlgDocColorsTab		: "颜色和边距",
DlgDocMetaTab		: "Meta 数据",

DlgDocPageTitle		: "页面标题",
DlgDocLangDir		: "语言方向",
DlgDocLangDirLTR	: "由左而右 (LTR)",
DlgDocLangDirRTL	: "由右而左 (RTL)",
DlgDocLangCode		: "语言代码",
DlgDocCharSet		: "字符编码",
DlgDocCharSetCE		: "中欧语系",
DlgDocCharSetCT		: "正体中文 (Big5)",
DlgDocCharSetCR		: "斯拉夫文",
DlgDocCharSetGR		: "希腊文",
DlgDocCharSetJP		: "日文",
DlgDocCharSetKR		: "韩文",
DlgDocCharSetTR		: "土耳其文",
DlgDocCharSetUN		: "Unicode (UTF-8)",
DlgDocCharSetWE		: "西欧语系",
DlgDocCharSetOther	: "其他字符编码",

DlgDocDocType		: "文件类型",
DlgDocDocTypeOther	: "其他文件类型",
DlgDocIncXHTML		: "包含 XHTML 定义",
DlgDocBgColor		: "背景颜色",
DlgDocBgImage		: "背景图像",
DlgDocBgNoScroll	: "浮水印",
DlgDocCText			: "文字",
DlgDocCLink			: "超链接",
DlgDocCVisited		: "已浏览过的超链接",
DlgDocCActive		: "活动超链接",
DlgDocMargins		: "页面边距",
DlgDocMaTop			: "上",
DlgDocMaLeft		: "左",
DlgDocMaRight		: "右",
DlgDocMaBottom		: "下",
DlgDocMeIndex		: "页面索引关键字 (用半形逗号[,]分隔)",
DlgDocMeDescr		: "页面说明",
DlgDocMeAuthor		: "作者",
DlgDocMeCopy		: "版权所有",
DlgDocPreview		: "预览",

// Templates Dialog
Templates			: "模板",
DlgTemplatesTitle	: "内容模板",
DlgTemplatesSelMsg	: "请选择编辑器内容模板<br> (原有的内容将会被清除):",
DlgTemplatesLoading	: "正在加载模板列表，请稍等...",
DlgTemplatesNoTpl	: "(没有模板)",
DlgTemplatesReplace	: "替换当前内容",

// About Dialog
DlgAboutAboutTab	: "关于",
DlgAboutBrowserInfoTab	: "浏览器信息",
DlgAboutLicenseTab	: "许可证",
DlgAboutVersion		: "版本",
DlgAboutInfo		: "想获得更多信息请至 ",

// Div Dialog
DlgDivGeneralTab	: "一般",
DlgDivAdvancedTab	: "高级",
DlgDivStyle		: "样式",
DlgDivInlineStyle	: "CSS 样式",

ScaytTitle			: "SCAYT",	//MISSING
ScaytTitleOptions	: "Options",	//MISSING
ScaytTitleLangs		: "Languages",	//MISSING
ScaytTitleAbout		: "About"	//MISSING
};
