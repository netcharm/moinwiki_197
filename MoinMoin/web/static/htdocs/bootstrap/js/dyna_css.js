﻿//
// copy css style routine
//
function getStyle(selector, style){
    var elemstr = '<div class="'+ selector +'"></div>';
    $elem = $(elemstr).hide().appendTo($("body"));
    var val = $elem.css(style);
    $elem.remove();
    return val;
}

$.fn.getStyleObject = function(){
    var dom = this.get(0);
    var style;
    var returns = {};
    if(window.getComputedStyle){
        var camelize = function(a,b){
            return b.toUpperCase();
        };
        style = window.getComputedStyle(dom, null);
        for(var i = 0, l = style.length; i < l; i++){
            var prop = style[i];
            var camel = prop.replace(/\-([a-z])/g, camelize);
            var val = style.getPropertyValue(prop);
            returns[camel] = val;
        };
        return returns;
    };
    if(style = dom.currentStyle){
        for(var prop in style){
            returns[prop] = style[prop];
        };
        return returns;
    };
    return this.css();
}

$.fn.copyCSS = function(source){
    //var elemstr = '<div class="'+ source +'"></div>';
    //$elem = $(elemstr).hide().appendTo($("body"));

    var styles = $(source).getStyleObject();
    this.css(styles);

    //$elem.remove();
}

//
// override search box behavior
//
function searchFocus(e) {
    // Update search input content on focus
    if (e.value == search_hint) {
        e.value = '';
        e.className = 'form-control';
        searchIsDisabled = false;
    }
}

function searchBlur(e) {
    // Update search input content on blur
    if (e.value == '') {
        e.value = search_hint;
        e.className = 'form-control disabled';
        searchIsDisabled = true;
    }
}

//
// override GUI editor menu item
//
function add_gui_editor_links() {
    // Add gui editor link after the text editor link

    // If the variable is not set or browser is not compatible, exit
    try {gui_editor_link_href}
    catch (e) {
        //alert("add_gui_editor_links: gui_editor_link_href not here");
        return
    }
    if (can_use_gui_editor() == false){
        //alert("add_gui_editor_links: can't use gui_editor");
        return;
    }

    var all = $('[name="texteditlink"]');
    all.each(function(){
        var textEditorLink = $(this);

        // Create a list item with a link
        var guiEditorLinkStr = '<a href="'+gui_editor_link_href+'">'+gui_editor_link_text+'</a>';
        var listItemStr = '<li class="list-group-item">'+guiEditorLinkStr+'</li>';
        $listItem = $(listItemStr);

        $listItem.insertAfter($(this.parentNode));
        //alert("add_gui_editor_links: added gui editor link");
    });
}

//function show_switch2gui() {
//    // Show switch to gui editor link if the browser is compatible
//    if (can_use_gui_editor() == false) return;
//
//    var switch2gui = $('#switch2gui');
//    if (switch2gui) {
//        switch2gui.addClass(list-group-item);
//        switch2gui.css('display', 'inline');
//    }
//}

//
// img display to center
//
// <style>
// .container     { position: relative; overflow: hidden; }
// .container img { position: absolute; }
// </style>
//
// <div class="container">
//     <img src="..."/>
// </div>
//
// <script type="text/javascript">
// $image = $('.container img');
// width = $image.width();
// height = $image.height();
//
// $image.css({
//     left: 0 - (width / 2),
//     top: 0 - (height / 2)
// });
// </script>

function Init_FormControl()
{
    // $('input[type="checkbox"]').each(function(){
    //     var containerString = '<div class="checkbox"></div>';
    //     $container = $(containerString).appendTo($(this.parentNode));
    //     $(this).next().appendTo($container);
    //     $(this).appendTo($container);
    // });

    // $('select').addClass("form-control input-sm");
    $('select').addClass("form-control");
    $("textarea").addClass("form-control");


    $('input[type="text"]').addClass("form-control");
    $('input[type="password"]').addClass("form-control");

    $('input[type="submit"]').addClass("btn btn-default");
    $('.advancedsearch').find('input[type="submit"]').addClass("form-control");
}

function Init_Image()
{
    $('img').addClass("img-rounded");
}

function Init_Table()
{
    $('table').addClass("table-bordered");
    $('form').find('table').addClass("table-hover");
}

function Init_Tooltip()
{
    // make tooltip
    $('img').tooltip();
    //$('img').popover(options={'trigger': 'hover'});

    $('a').not('[data-lightbox="images"], [data-lightbox="slide show"]').tooltip(options={'html':true});
}

function Init_IndexTable()
{
    var index = $(".table-of-contents");
    index.attr("id", "contentIndex");
    index.addClass("panel panel-default container");
    index.find('ul' ).addClass('nav');

    if( $('#preview, #previewbelow').length==0 )
    {
        var indexTableNodeStr = '<div class="nav navbar-inner" style="float: right;padding: 4px;"></div>';
        $indexTableNode = $(indexTableNodeStr).show().insertBefore($("#top"));

        index.appendTo($indexTableNode);
        index.css('max-width', '100%');
        index.attr('role', 'navigation');
    }
}

function Init_PageNameInDisc()
{
    var pageInDisc = $('#PageNameInDiscIcon')
    $.post( '?action=PageNameInDisc', '&mode=raw', function( data ) {
        // var content = data;
        // var content = data.match(/.{1,24}/g).join('<br />');
        var content = '<span style="word-wrap:break-word; overflow:hidden;">' + data + '</span>';
        pageInDisc.attr( 'data-content', content );
    });

    pageInDisc.on('show.bs.popover', function () {
        setTimeout(function () {
            pageInDisc.popover('hide');
        }, 10000);
    })
    pageInDisc.popover();
}

function Init_Tags()
{
    //$('.glyphicon-tags, .glyphicon-tag').popover(options={'placement':'left'});
    var tags = $('.glyphicon-tags, .glyphicon-tag');
    tags.popover();
    tags.on('show.bs.popover', function () {
          setTimeout(function () {
            tags.popover('hide');
        }, 5000);
    })

}

function Init_Carousel()
{
    $('.carousel').carousel();
}

function Init_ScrollSpy()
{
    //$("#page").scrollspy({ target:".table-of-contents" });
    //$("#page").scrollspy({ target:"#contentIndex" });
    //$('#contentIndex').scrollspy();

    //$('#contentIndex').affix();
}

function Init_DropDown()
{
    $('.dropdown-toggle').dropdown();
    // $('.dropdown-toggle').each(function() {
    //      /* iterate through array or object */
    //      $(this.parentNode).find('ul').addClass('dropdown-menu');
    //      $(this.parentNode).find('ol').addClass('dropdown-menu');
    // });
    // $('.dropdown-menu').find('ul').addClass('dropdown-menu');
    // $('.dropdown-menu').find('ol').addClass('dropdown-menu');
}

function Fixed_UI()
{
//    $(".thumbnail").attr("style", '');
    $("textarea").css('font-family', 'consolas,monaco');
    $("pre").css('font-family', 'consolas,monaco');

    // $("#content > p").css('text-indent', '2em');

    $(".mf-viral").hide();
}

function Init_ColorPicker()
{
    var isEditing = $('#IsEditing').val();
    if(isEditing == 1)
    {
        var jQueryTools = $('#jQueryTools');

        var ColorPickerString = '<li class="list-group-item"></li>';
        $ColorPicker = $(ColorPickerString).appendTo(jQueryTools);

        var pickerString = '<input type="text" id="colorpicker" data-color-format="hex">';
        $picker = $(pickerString).appendTo($ColorPicker);

        $picker.ColorPickerSliders({
            // connectedinput: '#colorpicker',
            flat: false,
            previewontriggerelement: true,
            color: '#cccccc',
            order: {
                hsl: 1,
                // cie: 1,
                // rgb: 1,
                // preview: 2
            },
            labels: {
                hslhue: '色调',
                hslsaturation: '色度',
                hsllightness: '亮度',

                rgbred: '红色',
                rgbgreen: '绿色',
                rgbblue: '蓝色',

                cielightness: '亮度',
                ciechroma: '色度',
                ciehue: '色调',

                opacity: '透明度',

                preview: '预览',
            },
            titleswatchesadd: "添加颜色色到常用列表",
            titleswatchesremove: "从常用列表移除颜色",
            titleswatchesreset: "重置为默认常用色表",
        });
    }
}

//
// Popup a Notify Infomation
//
function popup_notify(n_title, n_text, n_css, n_delay)
{
    n_delay = typeof n_delay !== 'undefined' ? n_delay : 10000;

    if(n_title && n_text)
    {
        $.pnotify.defaults.delay = n_delay;

        // n_text = $.trim(n_text).replace(/^\n/, '');
        n_text = $.trim(n_text);
        n_text = n_text.replace(/\n/ig, '');
        n_text = n_text.replace(/<br.*?\/>/ig, '');
        n_text = n_text.replace(/ +/ig, '<br />');
        n_text = '<span style="font:20px 华文楷体;">' + n_text + '</span>';

        var n_type = null;
        switch(n_css)
        {
            case 'btn-default':
                // n_type = 'info';
                n_type = '';
                break;
            case 'btn-info':
                n_type = 'info';
                break;
            case 'btn-success':
                n_type = 'success';
                break;
            case 'btn-warning':
                n_type = 'notice';
                break;
            case 'btn-danger':
                n_type = 'error';
                break;
        }

        $.pnotify({
            styling: "bootstrap",
            title: n_title,
            text: $.trim(n_text),
            addclass: 'ui-pnotify-title ' + n_css,
            // nonblock: true,
            // hide: false,
            closer: true,
            sticker: true,
            type: n_type,
        });
    }
}

var timer = null;
var CurrentActive = true;

function Checking_Notice()
{
    if(CurrentActive){
        // timer = window.clearTimeout(timer);

        $.post( '?action=Notice', '&mode=json', function( data ) {

            jsonData = $.parseJSON(data);

            var nt  = new Date;
            var date = nt.toDateString();
            var hour = nt.getHours();
            var min  = nt.getMinutes();

            $.each( jsonData, function(key, value) {
                if(key>=0)
                {
                    var st = new Date(date + ' ' + value.start);
                    var et = new Date(date + ' ' + value.stop);
                    if(et<st)
                    {
                        if(nt>=st)
                        {
                            et.setDate(et.getDate()+1);
                        }
                        else
                        {
                            st.setDate(st.getDate()-1);
                        }
                    }
                    if( (st <= nt) && (nt < et) )
                    {
                        TimeNotice = $('#TimeNotice');
                        // TimeNotice.text($.trim(value.text).replace(/^\n/, ''));
                        TimeNotice.text($.trim(value.text));
                        TimeNotice.attr('notify-title', value.title);
                        TimeNotice.attr('notify-text', $.trim(value.text));
                        TimeNotice.attr('notify-css', value.style);
                        TimeNotice.removeClass();
                        // alert(value.style);
                        TimeNotice.addClass('list-group-item ' + value.style);

                        popup_notify(value.title, value.text, value.style, 5000);
                    }
                }
            });
            // timer = window.setTimeout( Checking_Notice, 60000*1 );
        });
    }
}

function Init_Notice()
{
    if($("form#editor").length<=0){
        var jQueryTools = $('#jQueryTools');

        var noticeString = '<li class="list-group-item" id="TimeNotice" style="text-overflow:ellipsis;">Notices</li>';
        $TimeNotice = $(noticeString).appendTo(jQueryTools);
        $TimeNotice.on('click', function(){
           var title = $('#TimeNotice').attr('notify-title');
           var text  = $('#TimeNotice').attr('notify-text');
           var css   = $('#TimeNotice').attr('notify-css');
           popup_notify(title, text, css, 30000);
        });

        // Checking_Notice();

        $(window).focus(function() {
            // timer = window.clearTimeout(timer);
            CurrentActive =  true;
            // timer = window.clearInterval(timer);
            // timer = window.setInterval(Checking_Notice, 60000*1 );
            Checking_Notice();
        });

        $(window).blur(function() {
            // timer = window.clearTimeout(timer);
            CurrentActive =  false;
            // timer = window.clearInterval(timer);
        });
    }
}

//
// dynamic update some elements style for bootstrap
//
$(window).ready(function()
{

    Fixed_UI();

    Init_ColorPicker();

    Init_Notice();


    Init_FormControl();

    Init_Tags();
    Init_PageNameInDisc();

    Init_Tooltip();

    Init_Image();
    Init_Table();

    Init_IndexTable();
    Init_ScrollSpy();

    Init_DropDown();

    Init_Carousel();

});

