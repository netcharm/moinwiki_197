# -*- coding: utf-8 -*-
# wikiserverconfig_local.py
'''
local server config setting
'''

import os
import sys

from MoinMoin.config.multiconfig import DefaultConfig

from wikiserverconfig import LocalConfig

class Config(DefaultConfig):
    # docs = os.path.join(WIKIROOT, 'wiki')
    port = 9000

