#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    MoinMoin - rightsidebar theme

    Created by and for crw.
    Later it was rewritten by Nir Soffer for MoinMoin release 1.3.

    @copyright: 2005 Nir Soffer
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin import wikiutil, caching
from MoinMoin.theme import ThemeBase

from MoinMoin.Page import Page

class Theme(ThemeBase):

    name = "bootstrap"

    def bootstrap(self):
        """
        insert bootstrap files

        """
        bootstrap_fw = u"""
        <!-- Loading jQuery -->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="%(static)s/common/js/jquery-1.10.2.min.js"></script>
        <![endif]-->
        <!--[if gte IE 9]><!-->
        <script type="text/javascript" src="%(static)s/common/js/jquery-2.0.3.min.js"></script>
        <!--[endif]-->

        <!-- Loading bootstrap -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" charset="utf-8" media="screen" href="%(static)s/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" charset="utf-8" media="screen" href="%(static)s/bootstrap/css/bootstrap-theme.css">
        <script type="text/javascript" src="%(static)s/bootstrap/js/bootstrap.js"></script>

        <!-- Loading gallery plugin -->
        <link rel="stylesheet" type="text/css" charset="utf-8" media="all" href="%(static)s/gallery/css/text_x_gallery.css">
        <link type="text/css" rel="stylesheet" href="%(static)s/common/css/lightbox.css" media="screen"/>
        <script type="text/javascript" src="%(static)s/common/js/lightbox-2.6.min.js"></script>

        <!-- Loading modernizr -->
        <script type="text/javascript" src="%(static)s/common/js/modernizr.custom.js"></script>

        <!-- jquery.colorpickersliders -->
        <link rel="stylesheet" type="text/css" href="%(static)s/bootstrap/css/jquery.colorpickersliders.css"/>
        <script type="text/javascript" src="%(static)s/bootstrap/js/tinycolor.js"></script>
        <script type="text/javascript" src="%(static)s/bootstrap/js/jquery.colorpickersliders.js"></script>


        <!-- jquery.pnotify -->
        <link rel="stylesheet" type="text/css" href="%(static)s/bootstrap/css/jquery.pnotify.default.css"/>
        <link rel="stylesheet" type="text/css" href="%(static)s/bootstrap/css/jquery.pnotify.default.icons.css"/>
        <script type="text/javascript" src="%(static)s/bootstrap/js/jquery.pnotify.min.js"></script>

        <!-- Dynamic patch element class to bootstrap theme -->
        <script type="text/javascript" src="%(static)s/bootstrap/js/dyna_css.js"></script>

        <!-- MathJax Config Begin -->
        <script type="text/x-mathjax-config">
            MathJax.Hub.Config({
                /*showProcessingMessages: false,*/
                jax: ["input/TeX", "output/HTML-CSS"],
                TeX: {
                    TagSide: "left",
                    extensions: ["AMSmath.js", "AMSsymbols.js", "autobold.js", "boldsymbol.js", "color.js", "newcommand.js", "unicode.js", "verb.js"],
                    unicode: {
                        fonts: "STIXGeneral, 'Arial Unicode MS'",
                    },
                    Macros: {
                        /*RR: '{\\bf R}',
                        bold: ['{\\bf #1}',1]*/
                        sdash: '{|\\!\\!\\!\\sim}'
                    }
                },
                tex2jax: {
                    skipTags: ['script', 'noscript', 'style', 'textarea', 'pre', 'code', 'a'],

                    // ignoreClass: "[a-zA-Z1-9]*",
                    ignoreClass: 'Math_No',

                    processClass: 'MathTag',

                    // inlineMath: [ ['$','$'], ['\\(','\\)'] ],
                    inlineMath: [ ['$','$'] ],

                    // displayMath: [ ['$$','$$'], ['\\[','\\]'] ],

                    processEscapes: true
                }
            });
        </script>
        <script type="text/x-mathjax-config">
            MathJax.Hub.Queue(function() {
                var all = MathJax.Hub.getAllJax(), i;
                for(i=0; i < all.length; i += 1) {
                    all[i].SourceElement().parentNode.className += ' has-jax';
                }
            });
        </script>
        <script type='text/javascript' src='%(static)s/MathJax/MathJax.js?config=TeX-AMS-MML_HTMLorMML&input=ASCIIMath'></script>

        <!-- MathJax Config End -->

        """ % {"static": self.cfg.url_prefix_static}

        return bootstrap_fw

    def html_head(self, d):
        """ Assemble html head

        @param d: parameter dictionary
        @rtype: unicode
        @return: html head
        """
        html = [
            u'<title>%(title)s - %(sitename)s</title>' % {
                'title': wikiutil.escape(d['title']),
                'sitename': wikiutil.escape(d['sitename']),
            },
            self.externalScript('common'),
            self.headscript(d),
            self.guiEditorScript(d),
            self.html_stylesheets(d),
            self.rsslink(d),
            self.universal_edit_button(d),
            self.bootstrap(),
            ]
        return '\n'.join(html)

    def navibar(self, d):
        """ Assemble the navibar

        @param d: parameter dictionary
        @rtype: unicode
        @return: navibar html
        """
        request = self.request
        found = {}  # pages we found. prevent duplicates
        items = []  # navibar items
        item = u'<li class="%s list-group-item">%s</li>'
        current = d['page_name']

        # Process config navi_bar
        if request.cfg.navi_bar:
            for text in request.cfg.navi_bar:
                pagename, link = self.splitNavilink(text)
                if pagename == current:
                    cls = 'wikilink current'
                else:
                    cls = 'wikilink'
                items.append(item % (cls, link))
                found[pagename] = 1

        # Add user links to wiki links, eliminating duplicates.
        userlinks = request.user.getQuickLinks()
        for text in userlinks:
            # Split text without localization, user knows what he wants
            pagename, link = self.splitNavilink(text, localize=0)
            if not pagename in found:
                if pagename == current:
                    cls = 'userlink current'
                else:
                    cls = 'userlink'
                items.append(item % (cls, link))
                found[pagename] = 1

        # Add current page at end of local pages
        if not current in found:
            title = d['page'].split_title()
            title = self.shortenPagename(title)
            link = d['page'].link_to(request, title)
            cls = 'current'
            items.append(item % (cls, link))

        # Add sister pages.
        for sistername, sisterurl in request.cfg.sistersites:
            if sistername == request.cfg.interwikiname: # it is THIS wiki
                cls = 'sisterwiki current'
                items.append(item % (cls, sistername))
            else:
                # TODO optimize performance
                cache = caching.CacheEntry(request, 'sisters', sistername, 'farm', use_pickle=True)
                if cache.exists():
                    data = cache.content()
                    sisterpages = data['sisterpages']
                    if current in sisterpages:
                        cls = 'sisterwiki'
                        url = sisterpages[current]
                        link = request.formatter.url(1, url) + \
                               request.formatter.text(sistername) + \
                               request.formatter.url(0)
                        items.append(item % (cls, link))

        # Assemble html
        items = u''.join(items)
        html = u'''
        <ul id="navibar" class="list-group">
        %s
        </ul>
        ''' % items
        return html

    def editbar(self, d):
        """ Assemble the page edit bar.

        Create html on first call, then return cached html.

        @param d: parameter dictionary
        @rtype: unicode
        @return: iconbar html
        """
        _ = self.request.getText

        page = d['page']
        if not self.shouldShowEditbar(page):
            return ''

        html = self._cache.get('editbar')
        if html is None:
            # Remove empty items and format as list. The item for showing inline comments
            # is hidden by default. It gets activated through javascript only if inline
            # comments exist on the page.
            items = []
            for item in self.editbarItems(page):
                if item:
                    if 'nbcomment' in item:
                        # hiding the complete list item is cosmetically better than just
                        # hiding the contents (e.g. for sidebar themes).
                        items.append(u'<li class="toggleCommentsButton list-group-item" style="display:none;">%s</li>' % item)
                    else:
                        items.append(u'<li class="list-group-item">%s</li>' % item)

            #
            # append PageNameInDisc action menu item to panel
            #
            # item = u'<a id="PageNameInDisc" href="?action=PageNameInDisc" >%s</a>' % _("PageNameInDisc")
            # icon_title = u'%s' % ( _("PageNameInDisc") )
            # icon_class = u'glyphicon glyphicon-folder-open'
            # icon_id = u'PageNameInDiscIcon'
            # icon = u'<span id="%s" class="%s" data-placement="bottom" data-html="true" data-toggle="popover" title="%s"></span>' %  (icon_id, icon_class, icon_title)
            # items.append(u'<li class="list-group-item">%s %s</li>' % (item, icon))

            #
            # append Image2Attach action menu item to panel
            #
            item = u'<a id="Image2Attach" href="?action=Image2Attach" >%s</a>' % _("Image2Attach")
            icon_title = u'%s' % ( _("Image2Attach") )
            icon_class = u'glyphicon glyphicon-cloud-download pull-right'
            icon_id = u'Image2AttachIcon'
            icon = u'<span id="%s" class="%s" data-placement="bottom" data-html="true" data-toggle="popover" title="%s"></span>' %  (icon_id, icon_class, icon_title)
            items.append(u'<li class="list-group-item">%s %s</li>' % (item, icon))

            html = u'<ul class="editbar list-group">%s</ul>\n' % ''.join(items)
            self._cache['editbar'] = html

        return html

    def username(self, d):
        """ Assemble the username / userprefs link

        @param d: parameter dictionary
        @rtype: unicode
        @return: username html
        """
        request = self.request
        _ = request.getText

        userlinks = []
        # Add username/homepage link for registered users. We don't care
        # if it exists, the user can create it.
        if request.user.valid and request.user.name:
            interwiki = wikiutil.getInterwikiHomePage(request)
            name = request.user.name
            aliasname = request.user.aliasname
            if not aliasname:
                aliasname = name
            title = "%s @ %s" % (aliasname, interwiki[0])
            # link to (interwiki) user homepage
            homelink = (request.formatter.interwikilink(1, title=title, id="userhome", generated=True, *interwiki) +
                        request.formatter.text(name) +
                        request.formatter.interwikilink(0, title=title, id="userhome", *interwiki))
            userlinks.append(homelink)
            # link to userprefs action
            if 'userprefs' not in self.request.cfg.actions_excluded:
                userlinks.append(d['page'].link_to(request, text=_('Settings'),
                                                   querystr={'action': 'userprefs'}, id='userprefs', rel='nofollow'))

        if request.user.valid:
            if request.user.auth_method in request.cfg.auth_can_logout:
                userlinks.append(d['page'].link_to(request, text=_('Logout'),
                                                   querystr={'action': 'logout', 'logout': 'logout'}, id='logout', rel='nofollow'))
        else:
            query = {'action': 'login'}
            # special direct-login link if the auth methods want no input
            if request.cfg.auth_login_inputs == ['special_no_input']:
                query['login'] = '1'
            if request.cfg.auth_have_login:
                userlinks.append(d['page'].link_to(request, text=_("Login"),
                                                   querystr=query, id='login', rel='nofollow'))

        userlinks = [u'<li class="list-group-item">%s</li>' % link for link in userlinks]
        html = u'<ul id="username" class="list-group">%s</ul>' % ''.join(userlinks)
        return html

    def wikipanel(self, d):
        """ Create wiki panel """
        _ = self.request.getText
        html = [
            u'<div class="panel panel-primary">',
            u'<div class="panel-heading">%s</div>' % _("Wiki"),
            self.navibar(d),
            u'</div>',
            ]
        return u'\n'.join(html)

    def pagepanel(self, d):
        """ Create page panel """
        _ = self.request.getText
        if self.shouldShowEditbar(d['page']):
            icon_title = u'%s' % ( _("PageNameInDisc") )
            icon_class = u'glyphicon glyphicon-folder-open pull-right'
            icon_id = u'PageNameInDiscIcon'
            icon = u'<span id="%s" class="%s" data-container="body" data-placement="left" data-html="true" data-toggle="popover" title="%s" style="z-index:9999;"></span>' %  (icon_id, icon_class, icon_title)

            html = [
                u'<div class="panel panel-default">',
                u'<div class="panel-heading">%s&nbsp;%s</div>' % (_("Page"), icon),
                self.editbar(d),
                u'</div>',
                ]
            return u'\n'.join(html)
        return ''

    def userpanel(self, d):
        """ Create user panel """
        _ = self.request.getText

        html = [
            u'<div class="panel panel-info">',
            u'<div class="panel-heading">%s</div>' % _("User"),
            self.username(d),
            u'</div>'
            ]
        return u'\n'.join(html)

    def toolpanel(self, d):
        """ Create Tool panel """
        _ = self.request.getText

        isEditing = 1
        if self.shouldShowEditbar(d['page']):
            isEditing = 0

        if isEditing:
            icon_title = u'%s' % ( _("PageNameInDisc") )
            icon_class = u'glyphicon glyphicon-folder-open pull-right'
            icon_id = u'PageNameInDiscIcon'
            icon = u'<span id="%s" class="%s" data-container="body" data-placement="left" data-html="true" data-toggle="popover" title="%s" style="z-index:9999;"></span>' %  (icon_id, icon_class, icon_title)
        else:
            icon = u''

        html = [
            u'<div class="panel panel-default">',
            u'<div class="panel-heading">%s&nbsp;%s</div>' % (_("Tool"), icon),
            u'  <input id="IsEditing" type="hidden" value="%d">' % ( isEditing ),
            u'  <ul id="jQueryTools" class="list-group">',
            # u'    <li class="list-group-item">ColorPicker</li>',
            u'  </ul>',
            # self.editbar(d),
            u'</div>',
        ]
        return u'\n'.join(html)

    def startPage(self):
        """ Start page div with page language and direction

        @rtype: unicode
        @return: page div with language and direction attribtues
        """
        return u'<div id="page" class="panel panel-default Math_No"%s>\n' % self.content_lang_attr()

    def searchform(self, d):
        """
        assemble HTML code for the search forms

        @param d: parameter dictionary
        @rtype: unicode
        @return: search form html
        """
        _ = self.request.getText
        form = self.request.values
        updates = {
            'search_label': _('Search:'),
            'search_value': wikiutil.escape(form.get('value', ''), 1),
            'search_full_label': _('Text'),
            'search_title_label': _('Titles'),
            'url': self.request.href(d['page'].page_name)
        }
        d.update(updates)

        html = u'''
       <form id="searchform" class="navbar-form navbar-right form-inline" method="get" action="%(url)s">
           <div class="form-group">
               <input type="hidden" name="action" value="fullsearch">
               <input type="hidden" name="context" value="180">
               <div class="input-group input-group-sm">
                   <span class="input-group-addon">%(search_label)s</span>
                   <input id="searchinput" class="form-control" type="text" name="value" value="%(search_value)s" size="20" onfocus="searchFocus(this)" onblur="searchBlur(this)" onkeyup="searchChange(this)" onchange="searchChange(this)" alt="Search">
                   <span class="input-group-btn">
                       <input id="titlesearch" class="btn btn-default" name="titlesearch" type="submit" value="%(search_title_label)s" alt="Search Titles">
                       <input id="fullsearch" class="btn btn-default" name="fullsearch" type="submit" value="%(search_full_label)s" alt="Search Full Text">
                   </span>
               </div>
           </div>
       </form>
       <script type="text/javascript">
       <!--// Initialize search form
       var f = document.getElementById('searchform');
       // f.getElementsByTagName('label')[0].style.display = 'none';
       var e = document.getElementById('searchinput');
       searchChange(e);
       searchBlur(e);
       //-->
       </script>
       ''' % d
        return html

    def trail(self, d):
        """ Assemble page trail

        @param d: parameter dictionary
        @rtype: unicode
        @return: trail html
        """
        request = self.request
        user = request.user
        html = ''
        if not user.valid or user.show_page_trail:
            trail = user.getTrail()
            if trail:
                items = []
                for pagename in trail:
                    try:
                        interwiki, page = wikiutil.split_interwiki(pagename)
                        if interwiki != request.cfg.interwikiname and interwiki != 'Self':
                            link = (self.request.formatter.interwikilink(True, interwiki, page) +
                                    self.shortenPagename(page) +
                                    self.request.formatter.interwikilink(False, interwiki, page))
                            items.append('<li>%s</li>' % link)
                            continue
                        else:
                            pagename = page

                    except ValueError:
                        pass
                    page = Page(request, pagename)
                    title = page.split_title()
                    title = self.shortenPagename(title)
                    link = page.link_to(request, title)
                    items.append('<li><span class="label">%s</span></li>' % link)
                html = '''
                <ul id="pagetrail">
                %s
                </ul>''' % ''.join(items)
        return html

    def msg(self, d):
        """ Assemble the msg display

        Display a message with a widget or simple strings with a clear message link.

        @param d: parameter dictionary
        @rtype: unicode
        @return: msg display html
        """
        _ = self.request.getText
        msgs = d['msg']

        result = u""
        close = d['page'].link_to(self.request, text=_('Clear message'), css_class="clear-link")
        for msg, msg_class in msgs:
            try:
                result += u'<p>%s</p>' % msg.render()
                close = ''
            except AttributeError:
                if msg and msg_class:
                    result += u'<p><div class="%s">%s</div></p>' % (msg_class, msg)
                elif msg:
                    result += u'<p>%s</p>\n' % msg
        if result:
            html = result + close
            return u'<div id="message" class="alert alert-success">\n%s\n</div>\n' % html
        else:
            return u''

        return u'<div id="message" class="alert alert-info">\n%s\n</div>\n' % html

    def header(self, d):
        """
        Assemble page header

        @param d: parameter dictionary
        @rtype: string
        @return: page header html
        """
        _ = self.request.getText

        html = [
            # Custom html above header
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            u'<div id="header" class="navbar navbar-default" role="navigation">',
            self.logo(),
            self.searchform(d),
            u'<div id="locationline" class="navbar-default">',
            self.interwiki(d),
            self.title(d),
            u'</div>',
            self.trail(d),
            u'</div>',

            # Custom html below header (not recomended!)
            self.emit_custom_html(self.cfg.page_header2),

            # Sidebar
            u'''
            <div id="sidebar" class="col-md-3">
            ''',
            self.wikipanel(d),
            self.pagepanel(d),
            self.toolpanel(d),
            self.userpanel(d),
            u'''
            </div>
            ''',

            self.msg(d),

            # Page
            self.startPage(),

            ]
        return u'\n'.join(html)

    def editorheader(self, d):
        """
        Assemble page header for editor

        @param d: parameter dictionary
        @rtype: string
        @return: page header html
        """
        _ = self.request.getText

        html = [
            # Custom html above header
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            #u'<div id="header">',
            #self.searchform(d),
            #self.logo(),
            #u'</div>',

            # Custom html below header (not recomended!)
            self.emit_custom_html(self.cfg.page_header2),

            # Sidebar
            u'<div id="sidebar" class="col-md-3">',
            self.wikipanel(d),
            self.pagepanel(d),
            self.toolpanel(d),
            self.userpanel(d),
            u'</div>',

            self.msg(d),

            # Page
            self.startPage(),
            #self.title(d),
            ]
        return u'\n'.join(html)

    def footer(self, d, **keywords):
        """ Assemble wiki footer

        @param d: parameter dictionary
        @keyword ...:...
        @rtype: unicode
        @return: page footer html
        """
        page = d['page']
        html = [
            # End of page
            self.pageinfo(page),
            self.endPage(),

            # Pre footer custom html (not recommended!)
            self.emit_custom_html(self.cfg.page_footer1),

            # Footer
            u'<div id="footer">',
            self.credits(d),
            self.showversion(d, **keywords),
            u'</div>',


            # Post footer custom html
            self.emit_custom_html(self.cfg.page_footer2),
            ]
        return u'\n'.join(html)


def execute(request):
    """ Generate and return a theme object

    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """


    return Theme(request)

