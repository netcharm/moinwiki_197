#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
    MoinMoin - tex Parser using MathJax js library

    @copyright: 2012 luoboiqingcai <sf.cumt@163.com>
    @license: GNU GPL
"""

Dependencies = []

class Parser:
    """
        Send plain text in a HTML <pre> element.
    """

    ## specify extensions willing to handle
    ## should be a list of extensions including the leading dot
    ## TODO: remove the leading dot from the extension. This is stupid.
    #extensions = ['.txt']
    ## use '*' instead of the list(!) to specify a default parser
    ## which is used as fallback
    extensions = '.tex'
    Dependencies = ['page']

    def __init__(self, raw, request, **kw):
        self.raw = raw
        self.request = request
        self.form = request.form
        self._ = request.getText

    def format(self, formatter, **kw):
        """ Send the text. """
        # self.request.write(formatter.div(1))
        self.request.write(formatter.div(1, css_class='MathTag'))
        self.request.write(formatter.text(self.raw.expandtabs()))#expandtabs is str's buildin method
        self.request.write(formatter.div(0))
