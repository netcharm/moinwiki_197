import StringIO
import re
from MoinMoin.parser.text_moin_wiki import Parser as WikiParser
#from nocamelcase import Parser as WikiParser
from MoinMoin.formatter.text_docbook import Formatter as DocBookFormatter

class Parser:
    def __init__(self, raw, request, **kw):
        self.raw = raw
        self.request = request

    def _docbook(self, formatter):
        request = self.request
        tabs = self.raw.split('<<Tabs')[1:]

        formatter.table(True, attrs={'role': 'tabs'})
        for tab in tabs:
            title = "No Title"
            m = re.search("\((.*)\)>>", tab)
            if m:
                title = m.group(1)
            body = tab[tab.find(')>>')+3:]
            formatter.table_row(True)
            formatter.table_cell(True)
            WikiParser('<' + title + '>\n\n' + body, request).format(formatter)
            formatter.table_cell(False)
            formatter.table_row(False)
        formatter.table(False)

    def _wiki(self, formatter):
        result = []
        request = self.request
        if not hasattr(request, 'tabs_cnt'):
            request.tabs_cnt = 0
        tabs = self.raw.split('<<Tabs')[1:]

        result.append(formatter.div(True, css_class='tabs'))

        result.append(formatter.bullet_list(True))
        i = 0
        for tab in tabs:
            i += 1
            title = "No Title"
            m = re.search("\((.*)\)>>", tab)
            if m:
                title = m.group(1)
            result.append(formatter.listitem(True))
            result.append(formatter.url(True, '#tabs-%d' % (request.tabs_cnt + i)))
            result.append(formatter.text(title))
            result.append(formatter.url(False))
            result.append(formatter.listitem(False))
        result.append(formatter.bullet_list(False))

        i = 0
        for tab in tabs:
            i += 1
            body = tab[tab.find(')>>')+3:]
            result.append(formatter.div(True, id='tabs-%d' % (request.tabs_cnt + i)))
            strfile = StringIO.StringIO()
            request.redirect(strfile)
            try:
                WikiParser(body, request).format(formatter)
            finally:
                request.redirect()
            result.append(strfile.getvalue())
            result.append(formatter.div(False))

        result.append(formatter.div(False))

        request.tabs_cnt += len(tabs)

        request.write(''.join(result))

        if not hasattr(request, 'code_for_tabs_parser'):
            request.code_for_tabs_parser = True
            request.write("""
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" charset="utf-8" media="all" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
$(function () {
    $('.tabs').tabs();
});
</script>
""")

    def format(self, formatter):
        if formatter.__class__==DocBookFormatter:
            self._docbook(formatter)
        else:
            self._wiki(formatter)
