"""

    MoinMoin - Processor for turning long lists of data into simple
    tables for a more compact display.  Loosly based on CSV processor.

    (C) 2002 Robert Kleemann <robertk@oz.net>

    Data is considered to be line separated cells that contain wiki
    markup.  Each line will become a cell in a wiki table.  Lines may
    contain table markup (e.g. <:> for centered)

    Arguments are the following in any order

    integer      : number of columns for the table
    top-bottom   : fill rows from top to bottom
    bottom-top   : fill rows from bottom to top
    right-left   : fill rows from right to left
    left-right   : fill rows from left to right
    row-major    : fill rows first, columns second
    column-major : fill columns first, rows second
    sort         : alphanumerically sort the data before
                   filling the table

    mutually exclusive items are top-bottom/bottom-top,
    right-left/left-right and row-major/column-major.

    Default is: 2 top-bottom left-right row-major

    examples

    {{{#!FlowTable
    A
    B
    C
    D
    E
    }}}

    A B
    C D
    E

    {{{#!FlowTable 3 column-major
    ...

    A C E
    B D

    {{{#!FlowTable right-left bottom-top
    ...

      E
    D C
    B A

"""

import string, sys

from MoinMoin.parser import text_moin_wiki as wiki

# c'mon Guido, give us C's ternary operator
IF = lambda a,b,c:(a and [b] or [c])[0]

Dependencies = []

class Parser:
    extensions = '*'
    Dependencies = []
        

    def __init__(self, raw, request, **kw):
        self.raw = raw
        self.request = request
        self.form = request.form
        self.args = kw.get('format_args', '')
        self._ = request.getText

    def format(self, formatter):
        """ Send the text. """
        lines = self.raw.split('\n')

        # parse firstline arguments
        yo = "top-bottom"
        xo = "left-right"
        first = "row-major"
        sort = 0
        cols = 2
        for arg in string.split(self.args):
            if arg=="top-bottom" or arg=="bottom-top":
                yo=arg
            elif arg=="right-left" or arg=="left-right":
                xo=arg
            elif arg=="row-major" or arg=="column-major":
                first=arg
            elif arg=="sort":
                sort=1
            else:
                try:
                    cols = int(arg)
                except ValueError:
                    pass

        if sort:
            lines.sort()

        # make the new matrix in the correct order
        rows = (len(lines)-1) / cols + 1
        size = rows*cols
        matrix = []
        for i in range(size):
            matrix.append(" ")
        if yo=="top-bottom":
            if xo=="left-right":
                i=0
                if first=="row-major":
                    inc=lambda i,rows,cols,size: i+1
                else:
                    inc=lambda i,rows,cols,size: IF(i/cols==rows-1,i+1-(size-cols),i+cols)
            else:
                i=cols-1
                if first=="row-major":
                    inc=lambda i,rows,cols,size: IF(i%cols==0,i+cols*2-1,i-1)
                else:
                    inc=lambda i,rows,cols,size: IF(i/cols==rows-1,i-1-(size-cols),i+cols)
        else:
            if xo=="left-right":
                i=size-rows
                if first=="row-major":
                    inc=lambda i,rows,cols,size: IF(i%cols==cols-1,i-cols*2+1,i+1)
                else:
                    inc=lambda i,rows,cols,size: IF(i/cols==0,i+1+(size-cols),i-cols)
            else:
                i=size-1
                if first=="row-major":
                    inc=lambda i,rows,cols,size: i-1
                else:
                    inc=lambda i,rows,cols,size: IF(i/cols==0,i-1+(size-cols),i-cols)
        i2=0
        while i2<len(lines):
            matrix[i]=lines[i2]
            i = inc(i,rows,cols,size)
            i2=i2+1

        # create output list
        output = []
        for r in range(rows):
            row_of_cells = matrix[r*cols:(r+1)*cols]
            output.append("||"+string.join(row_of_cells,"||")+"||")

        wikiizer = wiki.Parser(string.join(output,'\n'),self.request)
        wikiizer.format(formatter)
