#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    MoinMoin - netcharm

    This parser is used to visualize markdown markup text

    @copyright: 2013 by netcharm
    @license: GNU GPL, see COPYING for details.
"""


from MoinMoin import caching, config, wikiutil

import markdown

Dependencies = []

exts = [
  'markdown.extensions.abbr',
  'markdown.extensions.admonition',
  'markdown.extensions.attr_list',
  'markdown.extensions.codehilite',
  'markdown.extensions.def_list',
  'markdown.extensions.footnotes',
  'markdown.extensions.headerid',
  'markdown.extensions.meta',
  'markdown.extensions.nl2br',
  'markdown.extensions.sane_lists',
  'markdown.extensions.smarty',
  'markdown.extensions.tables',
  'markdown.extensions.toc',
  'markdown.extensions.wikilinks',

  'pymdownx.github',
  'pymdownx.githubemoji',
  'pymdownx.arithmatex',
  'pymdownx.betterem',
  'pymdownx.caret',
  'pymdownx.extrarawhtml',
  'pymdownx.headeranchor',
  'pymdownx.inlinehilite',
  'pymdownx.magiclink',
  'pymdownx.mark',
  'pymdownx.progressbar',
  'pymdownx.smartsymbols',
  'pymdownx.superfences',
  'pymdownx.tasklist',
  'pymdownx.tilde',
  
]

class Parser:
    caching = 1
    extensions = ['.md']
    parsername = 'MarkDown'


    def __init__(self, raw, request, **kw):
        self.raw = raw
        self.request = request
        self.form = request.form

    def format(self, formatter):
        # html = markdown.markdown(self.raw)
        html = markdown.markdown(self.raw, extentions=exts)
        self.request.write(formatter.rawHTML(html))


