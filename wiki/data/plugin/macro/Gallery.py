#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    MoinMoin - Simple Gallery using lighybox 2.6

    This macro is used to call the Gallery parser,
    it is just a thin wrapper around it.

    Based on Gallery2/gallery2image, (c) 2013 by netcharm

    @copyright: 2013 by netcharm
    @license: GNU GPL, see COPYING for details.
"""
from MoinMoin import wikiutil

class Gallery:
    def __init__(self, macro, args):
        self.macro = macro
        self.formatter = macro.formatter
        self.args = args

    def renderInPage(self):
        _ = self.macro.request.getText

        gallery_parser = wikiutil.importPlugin(self.macro.cfg, 'parser', 'text_x_gallery', 'Parser')
        if gallery_parser is None:
            return self.formatter.text(_(u"Please install the Gallery parser!"))

        ap = gallery_parser("", self.macro.request, format_args=self.args)
        if ap.init_settings:
            return ap.render(self.formatter)

def execute(macro, args):
    return Gallery(macro, args).renderInPage()
