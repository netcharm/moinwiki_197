# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - ShortText macro

    It shows the first 180 chars of a wiki page without any processing instructions
    and only if the user is allowed to read that page.

    @copyright: 2005-2010 by MoinMoin:ReimarBauer
    @license: GNU GPL, see COPYING for details.
"""
Dependencies = ["pages"]

from MoinMoin.Page import Page
from MoinMoin import wikiutil

def macro_ShortText(macro, pagename=unicode, length=180):
    request = macro.request
    _ = request.getText
    if request.user.may.read(pagename):
        raw = Page(request, pagename).get_data()
        return request.formatter.text(raw[:length])
    else:
        return _("You are not allowed to view this page.")



