#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
MoinMoin - MathJax Macro

@copyright: 2012 luoboiqingcai <sf.cumt@163.com>
@license: GNU GPL
"""

Dependencies = ["pages"]

#def macro_MathJax(macro,rawtex):
#    "return the raw tex as it is."
#    return macro.request.formatter.text(rawtex)

def execute(macro, args):
    if not args:
        return ""
    else:
    	contents = u'%s' % args.strip().replace('"', '').replace('\'', '')
    	result   = u'<span class="MathTag"> %s </span>' % macro.request.formatter.text(contents)
        return( result )
