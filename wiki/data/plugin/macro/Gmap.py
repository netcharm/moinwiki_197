
import md5
"""
Gmap Macro
by Gavin Kou<gavin.kou@itbbq.com>
Output the JavaScript edition google map

<<Gmap(latitude,longitude,zoomLevel=10,width=600,height=600,type=2, marker=None)>>

<<Gmap(latitude,longtitude)>>

type:

0: ROADMAP
1: SATELLITE
2: HYBRID
3: TERRAIN

output sample:

<script type="text/javascript"  src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript" >
function initGmap_534af8a7d18890ac0b692cfd5f9abbbb() {
var latlng = new google.maps.LatLng(30.73239, 90.62073);
var myOptions = {
zoom: 10,
center: latlng,
mapTypeId: google.maps.MapTypeId.HYBRID
};
var map = new google.maps.Map(document.getElementById("534af8a7d18890ac0b692cfd5f9abbbb"), myOptions);
}
if (document.all) {
window.attachEvent('onload', initGmap_534af8a7d18890ac0b692cfd5f9abbbb );
} else {
window.addEventListener('load', initGmap_534af8a7d18890ac0b692cfd5f9abbbb , false);
</script>


more information  please refer to
http://code.google.com/intl/en/apis/maps/documentation/javascript/introduction.html

"""
def macro_Gmap(macro, latitude,longitude,zoomLevel=10,width=600,height=600,type=2,marker=None):
    div_id = md5.new( "%s,%s"% ( latitude, longitude) ).hexdigest()
    gmap_js_function = "initGmap_%s " % (div_id )
    gmap_type = ['ROADMAP','SATELLITE','HYBRID','TERRAIN']
    if marker:
        marker_js = """
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: %s
              }); """ % marker
    else:
        marker_js=""
        
    gmap_v3= """
    <script type="text/javascript"
    src="http://maps.google.com/maps/api/js?sensor=false">
    </script>
    <script type="text/javascript" defer="true" >
    function %(gmap_js_function)s(){
          var latlng = new google.maps.LatLng(%(latitude)s, %(longitude)s);
          var myOptions = {
          zoom: %(zoomLevel)s,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.%(gmap_type)s
          };
          var map = new google.maps.Map(document.getElementById("%(div_id)s"), myOptions);
          %(marker_js)s
          }

          if (document.all) {
          window.attachEvent('onload', %(gmap_js_function)s);
          } else {
          window.addEventListener('load', %(gmap_js_function)s, false);
          }

        </script>
        <div id="%(div_id)s" style="width: %(width)spx; height: %(height)spx"></div>            
    """ % {'div_id':div_id,
           'gmap_type':gmap_type[type],
           'latitude':latitude,
           'longitude':longitude,
           'zoomLevel':zoomLevel,
           'width':width,
           'gmap_js_function': gmap_js_function,
           'height':height,
           'marker_js':marker_js}
    return gmap_v3
