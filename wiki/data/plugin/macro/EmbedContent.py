﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
     MoinMoin - macro to embed content of an other (wiki) page
 
     <<EmbedContent(http://www.someplace.com/page.html)>>
     <<EmbedContent(file:c:\folder\file.txt)>>
	 http://moinmo.in/MacroMarket/EmbedContent
     @copyright: 2010 by MoinMoin:ReimarBauer
     @license: GNU GPL, see COPYING for details.
"""
import urllib2
 
def macro_EmbedContent(macro, url=u""):
    fmt = macro.formatter
    if url:
        uo = urllib2.urlopen(url)
        html = uo.read()
        uo.close()
        return fmt.rawHTML(html)