#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    MoinMoin - span generating macro

    Supported span attrs:
        class (use css_class param)
        id
        lang
        dir
        title

    Conditionally supported attrs:
        if cfg.span_supports_style is False (default):
        style - macro argument is accepted, but will be silently ignored, it
                won't create style attribute output.

        if cfg.span_supports_style is True (not default, DANGEROUS!):
        style - fully support span style attr, including potentially dangerous
                use of style. style attr value is too complex and browsers
                behave too differently to be able to filter this with
                reasonable effort. For details, please see:
                http://www.feedparser.org/docs/html-sanitization.html
                In short: can contain or load javascript, at least 2 different
                kinds of hiding stuff using character escaping, etc.

    Unsupported attrs:
        event attrs - unsafe, can contain javascript, XSS danger
        align - deprecated by the W3C (use css classes)

    Usage:
    <<span(red)>>some text contained in a span with css class red<<span>>
    <<span(css_class=red)>>same as above<<span>>
    <<span(id=foobar)>>some text in a span with id foobar<<span>>
    <<span(title="read this!")>>some text with a mouseover title<<span>>

    if cfg.span_supports_style is True, this also works:
    <<span(style="color: red; font: 20pt sans-serif;")>>20pt sans-serif red<<span>>

    @copyright: 2010 MoinMoin:ThomasWaldmann
    @license: GNU GPL, see COPYING for details.
"""

Dependencies = []


def macro_span(macro,
               # first the stuff we can directly give to span formatter:
               css_class=u'',
               id=u'',
               lang=u'',
               dir=u'',
               title=u'',
               # deprecated by W3C:
               #align=u'',
               style=u'',
              ):
    attrs = {}
    for key, value in [
        ('css_class', css_class),
        ('id', id),
        ('lang', lang),
        ('dir', dir),
        ('title', title),
        #('align', align),
        ]:
        if value:
            attrs[key] = value

    support_style = bool(getattr(macro.request.cfg, 'span_supports_style', False))
    if support_style:
        if style:
            attrs['style'] = style

    # if (some attr) or some style was given, we create an opening tag,
    # even if it was ONLY a style and we don't support style.
    if attrs or style:
        return macro.formatter.span(True, **attrs)
    else:
        return macro.formatter.span(False)

