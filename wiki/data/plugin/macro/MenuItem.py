#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    MoinMoin - div generating macro

    @copyright: 2010 MoinMoin:ThomasWaldmann
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin import wikiutil
from MoinMoin.parser.text_moin_wiki import Parser as WikiParser

Dependencies = []

def macro_MenuItem(macro,
               # first the stuff we can directly give to span formatter:
               content,
               css_class=u'',
               title=u'',
               style=u'',
              ):

    # print(content)
    # if (some attr) or some style was given, we create an opening tag,
    # even if it was ONLY a style and we don't support style.
    result = wikiutil.renderText(macro.request, WikiParser, content)

    attrs = u''
    if len(title)>0:
        attrs += u'title="%s" ' % title
    if len(css_class)>0:
        attrs += u'class="%s" ' % css_class
    if len(style)>0:
        attrs += u'style="%s" ' % style

    html = u'<li %s> %s </li>'% ( attrs, result )

    return(html)
    # return macro.formatter.rawHTML(html)

