#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
     MoinMoin - macro to Gallery suumery for page  pagename list

     @copyright: 2014 by netcharm
     @license: GNU GPL, see COPYING for details.
"""
import os
import sys

import codecs
import urllib

import string
import re

from MoinMoin import wikiutil
from MoinMoin.action import AttachFile
from MoinMoin.parser.text_moin_wiki import Parser as WikiParser

def macro_GallerySummary(macro, pagename=None, title=None, limit=4, size=96, width=-1, height=-1, link=False):
    fmt = macro.formatter
    if not pagename:
        pagename = macro.formatter.page.page_name

    if not title:
        title = pagename

    if width <= 0 and height <= 0:
        width = size
        height = size
    # if width == 0:
    #     width = size
    # if height == 0:
    #     height = size


    extensions = ['jpg', 'gif', 'png', 'jpeg', 'bmp', 'tif', 'tiff', 'svg']

    files = AttachFile._get_files(macro.request, pagename)
    files = [fn for fn in files if wikiutil.isPicture(fn) and not fn.startswith('tmp.')]
    if limit > len(files):
        limit = len(files)

    result = ''

    markups = []
    for f in files[:limit]:
        if width > 0 and height <= 0:
            imgSize = u'width=%d' % (width)
        elif width <= 0 and height > 0:
            imgSize = u'height=%d' % (height)
        elif width <= 0 and height <= 0:
            imgSize = u'height=%d' % (size)
        else:
            imgSize = u'width=%d,height=%d' % (width, height)

        if link:
            markup=u'[[%s|{{attachment:%s/%s||%s}}]]' % (title, pagename, f, imgSize)
        else:
            markup=u'{{attachment:%s/%s||%s}}' % (pagename, f, imgSize)
        markups.append(markup)

    markup = ' '.join(markups)
    result = wikiutil.renderText(macro.request, WikiParser, markup)

    return(fmt.rawHTML(result))

