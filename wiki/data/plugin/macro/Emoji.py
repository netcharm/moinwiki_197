#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Emoji macro

by NetCharm

"""

#Dependencies = ["pages"]
#def execute(macro, emoji, size='1.0em', color=None):
#    text = macro.request.formatter.text(emoji.strip())
#    if color == None:
#        result = u'<span style=\"font-family:\'Segoe UI Emoji\'; font-size:%s;\">%s</span>' % (text, size)
#    else:
#        result = u'<span style=\"font-family:\'Segoe UI Emoji\'; font-size:%s; color:%s;\">%s</span>' % (text, size, color)
#    return result 

def macro_Emoji(macro, emoji, size='1.0em', font='Segoe UI Emoji', color=None):
    text = macro.request.formatter.text(emoji.strip())
    if font == None: font = 'Segoe UI Emoji'
    if color == None:
        result = u'<span style=\"font-family:\'%s\'; font-size:%s;\">%s</span>' % (font.strip('\''), size.strip('\''), text)
    else:
        result = u'<span style=\"font-family:\'%s\'; font-size:%s; color:%s;\">%s</span>' % (font.strip('\''), size.strip('\''), color.strip('\''), text)
    return macro.formatter.rawHTML(result)
    
def macro_emoji(macro, emoji, size='1.0em', font='Segoe UI Emoji', color=None):
    text = macro.request.formatter.text(emoji.strip())
    if font == None: font = 'Segoe UI Emoji'
    if color == None:
        result = u'<span style=\"font-family:\'%s\'; font-size:%s;\">%s</span>' % (font.strip('\''), size.strip('\''), text)
    else:
        result = u'<span style=\"font-family:\'%s\'; font-size:%s; color:%s;\">%s</span>' % (font.strip('\''), size.strip('\''), color.strip('\''), text)
    return macro.formatter.rawHTML(result)
    

    