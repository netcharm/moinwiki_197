#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    MoinMoin - div generating macro

    @copyright: 2010 MoinMoin:ThomasWaldmann
    @license: GNU GPL, see COPYING for details.
"""

Dependencies = []


def macro_Menu(macro,
               # first the stuff we can directly give to span formatter:
               label,
               id=u'',
               css_class=u'btn-default',
               title=u'',
               style=u'',
              ):

    # if (some attr) or some style was given, we create an opening tag,
    # even if it was ONLY a style and we don't support style.
    attrs = u''

    if style:
      result += u'style="%s" ' % style
    if title:
      result += u'title="%s" ' % title
    if id:
      result += u'id="%s" ' % id

    # print('DropDown Menu')
    if label:
          # <a class="btn %s" role="button" data-toggle="dropdown" data-target="#"> %s <span class="caret"></span></a>
        html = u'''
        <div class="btn-group">
          <button type="button" class="btn %s dropdown-toggle" data-toggle="dropdown" > %s <span class="caret"></span></button>
          <ul class="dropdown-menu" role="menu" aria-labelledby="%s" %s >
        ''' % (css_class, label, label, attrs)
    else:
        html = u'''        
          </ul>
        </div>
        '''

    # if label:
    #       # <a class="btn %s" role="button" data-toggle="dropdown" data-target="#"> %s <span class="caret"></span></a>
    #     html = u'''
    #     <div class="btn-group">
    #       <button type="button" class="btn %s dropdown-toggle" data-toggle="dropdown" > %s <span class="caret"></span></button>
    #     ''' % (css_class, label)
    # else:
    #     html = u'''        
    #     </div>
    #     '''

    return(html)
    # return macro.formatter.rawHTML(html)

