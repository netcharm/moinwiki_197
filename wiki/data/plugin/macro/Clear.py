#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
     MoinMoin - macro to Force breakline to new display block/clear the image alignment effect

     @copyright: 2013 by netcharm
     @license: GNU GPL, see COPYING for details.
"""

def macro_Clear(macro, value=u"left"):
    fmt = macro.formatter

    # print(value)
    values = [u'left', u'right', u'both', u'none']
    value = value.lower()

    result = u''
    if value in values:
    	result = u'<div style="clear:%s;"></div>' % value
    
    return(fmt.rawHTML(result))



