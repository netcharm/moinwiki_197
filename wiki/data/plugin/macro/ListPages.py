#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    MoinMoin - ListPages

    print a list of pages whose title matches the filter term of getPageList

    @copyright: @copyright: 2001-2003 Juergen Hermann <jh@web.de>,
                2003-2008 MoinMoin:ThomasWaldmann,
                2009-2011 MoinMoin:ReimarBauer
    @license: GNU GPL, see COPYING for details.
"""

Dependencies = ["namespace"]

import re
from MoinMoin import search, wikiutil, user
from MoinMoin.Page import Page

def _subpage(pagename):
    """
    returns the last last segement of a pagename
    @param pagename: the pagename
    """
    segments = pagename.split('/')
    return segments[-1]

def macro_ListPages(macro, search_term=u'.+', list_type=("number_list", "bullet_list", "content"),
                    link=("pagename", "subpage"), username=u'', reverse=False):
    """
    lists pages
    @param search_term: regex for searching of page titles
    @param list_type: select type of list in between number_list and bullet_list
    @param link: change the link from pagename to subpage for shorter links
    @param reverse: reverse order
    """
    _ = macro._
    case = 0

    # With whitespace argument, return same error message as FullSearch
    if not search_term.strip():
        msg = _('Please use a more selective search term instead of {{{"%s"}}}', wiki=True) % search_term
        return '<span class="error">%s</span>' % msg

    # Return a title search for needle, sorted by name.
    # based on rootpage.getPageList because search does also list attachments
    try:
        pageuser = user.User(macro.request, id=user.getUserId(macro.request, username))
        print(pageuser)

        rootpage = macro.request.rootpage
        filterfn = re.compile(ur"%s" % search_term, re.U).match
        pageobjs = rootpage.getPageList(user=pageuser, include_underlay=False, exists=1, filter=filterfn, return_objects=True)
        f = macro.request.formatter
        if link == "pagename":
            ret = [''.join([f.listitem(1),
                   f.url(1, href=page.url(macro.request, escape=0)),
                   page.page_name,
                   f.url(0)]) for page in pageobjs]
        if link == "subpage":
             ret = [''.join([f.listitem(1),
                   f.url(1, href=page.url(macro.request, escape=0)),
                   _subpage(page.page_name),
                   f.url(0)]) for page in pageobjs]

        ret.sort()
        if reverse:
            ret.reverse()
        if list_type == "number_list":
            ret = ''.join([f.number_list(1), ''.join(ret), f.number_list(0)])
        if list_type == "bullet_list":
            ret = ''.join([f.bullet_list(1), ''.join(ret), f.bullet_list(0)])

        if list_type == "content":
            pagenames = [page.page_name for page in pageobjs]
            pagenames.sort()
            if reverse:
                pagenames.reverse()
            for pagename in pagenames:
                macro.request.page = Page(macro.request, pagename)
                # renders selected pages content
                Page(macro.request, pagename).send_page(emit_headers=False, content_only=True)
            ret = ""

    except ValueError:
        # same error as in MoinMoin/action/fullsearch.py, keep it that way!
        ret = ''.join([macro.formatter.text('<<PageList('),
                      _('Your search query {{{"%s"}}} is invalid. Please refer to '
                        'HelpOnSearching for more information.', wiki=True,
                        percent=True) % wikiutil.escape(search_term),
                      macro.formatter.text(')>>')])
    return ret
