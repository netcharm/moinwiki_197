// v1.3-1.5
var seesawc, seesaws, seesawfx = {
    toggle: {slide:"slideToggle", fade:"fadeToggle", show:"toggle", hide:"toggle"},
    show:  {slide:"slideDown", fade:"fadeIn", show:"hide", hide:"show"},
    hide: {slide:"slideUp", fade:"fadeOut", show:"show", hide:"hide"}
};

function seeSaw(sections, effect, speed, seesaw) {
    var dt, fx;

    $.each(["toggle","show","hide"], function() {
	fx = seesawfx[this]; fx.dflt = this;
	$.each(sections[this], function() {
	    seesawc.filter((dt = "." + this)).find(".part").not(".seesawed").addClass("seesawed").filter(".show")[fx.show]().end().filter(".hide")[fx.hide]();
	    (seesaw ? seesaws.filter(dt) : $(dt)).not(".seesawc,.seesawed").addClass("seesawed").each(function() {
		speed ? $(this)[fx[effect]](speed) : $(this)[fx[effect]](); 
	    });
	});
    });
    $(".seesawed").removeClass("seesawed");
};

$(document).ready(function() {
    var data;

    (seesaws = $(".seesaw")).not(".show").hide();
    (seesawc = $(".seesawc")).each(function() {
	if ((data = $(this).data()))
	    seesaws.filter("." + data.section + "-bg").css("background-color", data.bg);
    });
});
