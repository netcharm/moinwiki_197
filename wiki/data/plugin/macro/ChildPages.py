# -*- coding: iso-8859-1 -*-
"""
    ChildPages Macro

    List first-level subpages for the current page.

    @copyright: 2009 Renato Silva
    @license: GNU GPLv2
"""

Dependencies = ["namespace"]
from MoinMoin import search, wikiutil
from MoinMoin.search.results import FoundAttachment
import re

def single_name(page_name):
    return page_name.split('/')[-1]

def cut_name(text, max_length):
    if max_length < 1:
        return text
    return re.compile('^(.{%d}).+$' % max_length).sub(ur'\1...', text)

def spaced_name(wiki_name):
    wiki_name = re.compile('([^A-Z\s])([A-Z])').sub(ur'\1 \2', wiki_name)
    wiki_name = re.compile('(\d)(\D)').sub(ur'\1 \2', wiki_name)
    wiki_name = wiki_name.replace('_', ' ')
    return wiki_name.replace('/', ' /')

def format_name(page_name, max_length, add_spaces):
    page_name = single_name(page_name)
    if add_spaces:
        page_name = spaced_name(page_name)
    return cut_name(page_name, max_length)

def format_macro_title(title, page_name):
    if title.count('%s') == 1:
        title = title %  page_name
    return title

def action_allowed(request, on_actions):
    action_listed = lambda request: request.action in on_actions.split('|')
    if on_actions.startswith('not:'):
        on_actions = on_actions[4:]
        allowed = lambda: not action_listed(request)
    elif on_actions != 'all':
        allowed = lambda: action_listed(request)
    else:
        allowed = lambda: True
    return allowed()

def child_regex(page_name):
    for old, new in [(' ', '\s'), ('?', '\?'), (':', '.')]:
        prepared_page_name = page_name.replace(old, new)
    return u'regex:^%s/[^/]+$' % prepared_page_name

def macro_ChildPages(macro, on=None, title=None, none_note=None, more_link=None, max_pages=0, max_name=0, use_list=True, spaces=True):
    if not more_link: more_link = macro._('List all...')
    if not on: on = 'show'
    request = macro.request
    fmt = macro.formatter
    page = request.page if fmt.page.page_name == 'SideBar' else fmt.page
    if not action_allowed(request, on):
        return ''

    output = []
    append = output.append
    def append_item(item, use_list=use_list):
        tag = fmt.listitem if use_list else fmt.paragraph
        append(tag(1))
        append(item)
        append(tag(0))

    if title:
        page_name = format_name(page.page_name, max_name, spaces)
        append(format_macro_title(title, page_name))

    query = child_regex(page.page_name)
    found_children = search.searchPages(request, query, titlesearch=1, case=0, sort='page_name').hits
    found_children = [child for child in found_children if not isinstance(child, FoundAttachment)]

    if found_children:
        too_many_children = (max_pages > 0 and len(found_children) > max_pages)
        if too_many_children: found_children = found_children[:max_pages]

        append(fmt.div(1, css_class='searchresults'))
        if use_list: append(fmt.bullet_list(1))

        for found_child in found_children:
            child = found_child.page
            append_item(child.link_to(request, text=format_name(child.page_name, max_name, spaces)))

        if too_many_children: append_item(page.link_to(request, text=more_link, querystr={'action': 'LocalSiteMap'}))
        if use_list: append(fmt.bullet_list(0))
        append(fmt.div(0))
    elif none_note != None:
        append_item(none_note, use_list=use_list)
    else:
        return ''
    return ''.join(output)
