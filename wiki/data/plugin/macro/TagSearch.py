#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import urllib
import urllib2
import urlparse
import time

from MoinMoin import log
logging = log.getLogger(__name__)

from MoinMoin import config, wikiutil, search
from MoinMoin.search.results import Match, TitleMatch, TextMatch
from MoinMoin.Page import Page


def splitTags(tagstring):
    # print(tagstring)
    # taglist = tags.split('.,;:-|/ ')
    taglist = []
    #print(re.findall(r"[\w']+", tags))
    taglist.extend(re.findall(r"[\w']+", tagstring))
    #print(re.findall(u"[\x80-\xff]+", tags))
    taglist.extend(re.findall(u"[\x80-\xff]+", tagstring.encode('utf8')))

    return taglist

def unquote(strlist):
    return map(urllib2.unquote, strlist)

def getPageByTag(macro, taglist):
    fmt = macro.formatter

    # regex:'<<Tags\(.*?中文.*?\)>>' regex:'<<Tags\(.*?一二.*?\)>>') -> regex%3A%27%3C%3CTags%5C%28.*%3F
    # tagTemplate = '%%27%%3C%%3CTags%%5C%%28.*%%3F%s.*%%3F%%5C%%29%%3E%%3E%%27'
    tagTemplate = "'<<Tags\(.*?%s.*?\)>>'"
    # print(tagTemplate)


    tagsLinks = []
    for tag in taglist:
        # tagsLinks.append(tagTemplate % urllib2.quote(tag))
        tagsLinks.append(tagTemplate % tag)
    if len(tagsLinks) == 0:
        tagsLinks.append("'<<Tags\(.*?\)>>'")


    pageurls = u''

    pagelist = []
    # print(macro.request.http_referer)
    url_root = macro.request.http_referer
    if url_root:
        search_action    = u'action=fullsearch&advancedsearch=1'
        search_and_terms = u'&and_terms=%s' % (u'+'.join(tagsLinks))
        search_or_terms  = u'&or_terms=%s' % (u'')
        search_not_terms = u'&not_terms=%s' % (u'')

        # &categories=&language=&mimetype=&excludeunderlay=1&nosystemitems=1
        search_mtime           = u'&mtime=%s'           % (u'')
        search_categories      = u'&categories=%s'      % (u'')
        search_language        = u'&language=%s'        % (u'')
        search_mimetype        = u'&mimetype=%s'        % (u'text')
        search_excludeunderlay = u'&excludeunderlay=%s' % (u'1')
        search_nosystemitems   = u'&nosystemitems=%s'   % (u'1')

        searchLink = u'''
        %(ref)s?%(action)s%(and)s%(or)s%(not)s%(mtime)s%(categories)s%(language)s%(mimetype)s%(excludeunderlay)s%(nosystemitems)s
        ''' % {
            'ref'               : url_root,
            'action'            : search_action,
            'and'               : search_and_terms,
            'or'                : search_or_terms,
            'not'               : search_not_terms,
            'mtime'             : search_mtime,
            'categories'        : search_categories,
            'language'          : search_language,
            'mimetype'          : search_mimetype,
            'excludeunderlay'   : search_excludeunderlay,
            'nosystemitems'     : search_nosystemitems,
        }
        #print(u'Search Link: %s' % searchLink)
        #print(u'Page Name: %s' % fmt.page.page_name)

        queryString = u'+'.join(tagsLinks)

        # results = search.searchPages(macro.request, search_and_terms, regex=True, sort='page_name')
        results = search.searchPages(macro.request, queryString, regex=True, sort=u'page_name')
        # results = search.getSearchResults(macro.request, queryString, regex=True, sort='page_name')

        pagelist = results.pageList(macro.request, macro.formatter, paging=False)
        # print(pagelist)

        pageurls = re.findall('href=[\'|"](\/.*?)\?', pagelist)
        pageurls = list(set(pageurls))
        # print(pageurls)

    return(pageurls)
    pass

def makeTagLink(macro, tag, tagValue):
    _ = macro.request.getText
    fmt = macro.formatter

    search_mtime           = u'&mtime=%s'           % (u'')
    search_categories      = u'&categories=%s'      % (u'')
    search_language        = u'&language=%s'        % (u'')
    search_mimetype        = u'&mimetype=%s'        % (u'text')
    search_excludeunderlay = u'&excludeunderlay=%s' % (u'1')
    search_nosystemitems   = u'&nosystemitems=%s'   % (u'1')

    searchLink = u'?action=fullsearch&advancedsearch=1&and_terms=regex%%3A%%27%%3C%%3CTags\(.*%%3F%s.*%%3F\)%%3E%%3E%%27'+search_mimetype+search_excludeunderlay+search_nosystemitems

    link  = searchLink % tag.encode('utf8')
    title = u'<br/>'.join(tagValue)
    badge = u'<span class="badge">%d</span>' % len(tagValue)
    title = _(u'Total %d pages used this tag.') % len(tagValue)
    # css   = u"btn btn-default glyphicon glyphicon-tag"
    # css   = u"btn-default glyphicon glyphicon-tag"
    # css   = u"alert glyphicon glyphicon-tag"
    css   = u"alert glyphicon"
    # css   = u"alert"
    # tagLink = fmt.url(True, href=link, title=title, css_class=css) + \
    #           fmt.rawHTML(badge) + \
    #           fmt.text(tag) + \
    #           fmt.url(False)
    # tagLink = fmt.url(True, href=link, title=title, css_class=css) + \
    #           tag + \
    #           badge + \
    #           fmt.url(False)
    tagLink = fmt.url(True, href=link, title=title, css_class=css) + \
              tag + \
              fmt.url(False)
    return(tagLink)

def makeTagStyle(tagLink, tagCount, totalCount):
    #{level:hits , level:hits , ...}
    level = { 0 : 4 , 1 : 7 , 2 : 12 , 3 : 18 , 4 : 25 , 5 : 35 , 6 : 50 , 7 : 60 , 8 : 90 }
    hits = (tagCount+0.0) / (totalCount+0.0) *100
    # print(tagCount, totalCount, hits)

    tagSize = 1.00
    tagClass = 0
    #level0
    if hits < level[0]:
        # tagSize = 0.80
        tagClass = 0
    #level1
    elif hits < level[1]:
        # tagSize = 1.20
        tagClass = 1
    #level2
    elif hits < level[2]:
        # tagSize = 1.40
        tagClass = 2
    #level3
    elif hits < level[3]:
        # tagSize = 1.60
        tagClass = 3
    #level4
    elif hits < level[4]:
        # tagSize = 1.80
        tagClass = 4
    #level5
    elif hits < level[5]:
        # tagSize = 2.00
        tagClass = 5
    #level6
    elif hits < level[6]:
        # tagSize = 2.20
        tagClass = 6
    #level7
    elif hits < level[7]:
        # tagSize = 2.40
        tagClass = 7
    #level8
    elif hits < level[8]:
        # tagSize = 2.60
        tagClass = 8
    #level9
    else:
        # tagSize = 2.80
        tagClass = 9

    # html = u'<span style="font-size:%.2fem;" class="tag-cloud tag-l%d">%s</span>&nbsp;\n' % (tagSize, tagClass, tagLink)
    html = u'<span class="tag-cloud tag-l%d">%s</span>&nbsp;\n' % (tagClass, tagLink)

    return(html)

def getTagFromPage(macro, pagename):
    fmt = macro.formatter
    # print(pagename.decode('utf8'))
    if macro.request.user.may.read(pagename):
        page = Page(macro.request, pagename)
        page_raw = page.get_raw_body()
    else:
        page_raw =u''

    page_tags = re.findall(r'<<Tags\((.*?),*?\)>>', page_raw)
    page_taglist = []
    for tag in page_tags:
        tag_clean = re.sub(r', *expand=.*', '', tag)
        page_taglist.extend(splitTags(tag_clean))
    page_taglist = list(set(page_taglist))
    # print(page_taglist)

    return(page_taglist)

def getTagFromPages(macro, pagelist):
    fmt = macro.formatter

    all_tags = dict()
    for url in pagelist:
        pagename = urllib2.unquote(url.encode('ascii')).encode('utf8')[1:]
        page_taglist = getTagFromPage(macro, pagename)
        for tag in page_taglist:
            if all_tags.has_key(tag.decode('utf8')):
                all_tags[tag.decode('utf8')].append(urllib.unquote_plus(url.encode('ascii')).decode('utf8'))
            else:
                all_tags[tag.decode('utf8')] = [urllib.unquote_plus(url.encode('ascii')).decode('utf8')]

    totalPages = []
    totalCount = 0
    for tag in all_tags:
        totalPages.extend(all_tags[tag])
        totalCount += len(all_tags[tag])
    totalCount = len(list(set(totalPages)))

    tagLinks = []
    for tag in all_tags:
        tagCount = len(all_tags[tag])
        tagLink = makeTagLink(macro, tag, all_tags[tag])
        tagLink = makeTagStyle(tagLink, tagCount, totalCount)
        tagLinks.append(tagLink)

    result = ' '.join(tagLinks)

    return(result, len(tagLinks))
    pass

def macro_TagSearch(macro, tags=None, page=None, limit=0):
    _ = macro.request.getText
    fmt = macro.formatter
    # print(dir(fmt))

    if not tags:
        tags = ''

    #print(tags)

    st = time.clock()

    result = ''


    if page:
        pagelist = [u'/%s' % urllib.quote(page.encode('utf8'))]
    else:
        taglist = splitTags(tags)
        pagelist = getPageByTag(macro, taglist)

    if len(tags) > 0:
        # result = fmt.rawHTML('\n'.join(pagelist))
        result += fmt.rawHTML(pagelist)
    else:
        alltags = getTagFromPages(macro, pagelist)
        result += alltags[0]
        # print(pagelist)

    timing = _(u'This query cost % 8.4fs. Got %d tags.<br />\n') % (time.clock() - st, alltags[1])

    return(fmt.rawHTML(timing) + fmt.rawHTML(result))

    # return(result)



