#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    MoinMoin - RandomBlockQuote Macro

    Selects a random quote multiine from FortuneCookies or a given page.

    Usage:
        <<RandomQuote()>>
        <<RandomQuote(WikiTips)>>
        <<RandomQuote(WikiTips, split)>>

    Comments:
        It will look for list delimiters on the page in question.
        It will ignore anything that is not in an "*" list.

    @copyright: 2013 netcharm
    @license: GNU GPL, see COPYING for details.

    Originally written by Thomas Waldmann.
    Gustavo Niemeyer added wiki markup parsing of the quotes.
"""

import random
import re

from MoinMoin.Page import Page

Dependencies = ["time"]

def macro_RandomBlockQuote(macro, pagename=u'FortuneCookies', split=u'----'):
    _ = macro.request.getText

    if macro.request.user.may.read(pagename):
        page = Page(macro.request, pagename)
        raw = page.get_raw_body()
    else:
        raw = ""

    quotes = re.split('\n'+split.replace('\'', ''), raw)

    quotes = [quote.strip() for quote in quotes]
    quotes = [quote.replace('* ', '') for quote in quotes]
    quotes = [quote.replace(split, '') for quote in quotes]
    # quotes = [quote[2:] for quote in quotes if quote.startswith('* ')]

    if not quotes:
        return (macro.formatter.highlight(1) +
                _('No quotes on %(pagename)s.') % {'pagename': pagename} +
                macro.formatter.highlight(0))

    quote = random.choice(quotes).strip()
    page.set_raw_body(quote, 1)
    quote = macro.request.redirectedOutput(page.send_page,
        content_only=1, content_id="RandomBlockQuote")

    return quote
