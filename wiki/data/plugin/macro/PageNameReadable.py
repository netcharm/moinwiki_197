﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

"""
     MoinMoin - macro to Readable pagename list

     @copyright: 2013 by netcharm
     @license: GNU GPL, see COPYING for details.
"""
from MoinMoin import wikiutil

def getfoldername(fn_encoded):
    fn_decoded = wikiutil.unquoteWikiname(fn_encoded)
    return(fn_decoded)

def getfoldernames(CWD=None):
    if not CWD:
      CWD = os.getcwd()

    # result = os.path.walk(CWD, getfoldername)
    dirs_encoded = os.listdir(CWD)
    dirs_decoded = map(getfoldername, dirs_encoded)

    return(dirs_decoded, dirs_encoded)

def PageToFolder(page):
    fn_encoded = wikiutil.quoteWikinameFS(page)
    return(fn_encoded)

def macro_PageNameReadable(macro, pagename=None, foldername=None):
    fmt = macro.formatter
    # print(dir(fmt))
    # print(pagename, foldername)

    if pagename and (not foldername):
        item = fmt.text(PageToFolder(pagename))
        return(item)
    elif foldername and (not pagename):
        item = fmt.text(getfoldername(foldername))
        return(item)
    elif (not pagename) and (not foldername):
        pagenames = getfoldernames(os.path.join(macro.request.cfg.data_dir, 'pages'));
        pagenames = [list(a) for a in zip(pagenames[0], pagenames[1])]
        items = []
        for item in pagenames:
            link  = item[0]
            desc  = item[0]
            title = item[1]
            text  = fmt.url(True, href=link, title=title) + \
                    fmt.text(desc) + \
                    fmt.url(False)
            items.append( text )

        #return fmt.rawHTML('<br />\n'.join(items))
        return (u'<br />\n'.join(items))
    else:
        return ('')






