# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - mathtran

    This macro is used to call the mathtran parser,
    it is just a thin wrapper around it.

    @copyright: 2008-2009 by MoinMoin:ReimarBauer
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin import wikiutil

class mathtran:
    def __init__(self, macro, args):
        self.macro = macro
        self.formatter = macro.formatter
        self.args = args

    def renderInPage(self):
        mathtran_parser = wikiutil.importPlugin(self.macro.cfg, 'parser', 'text_x_mathtran', 'Parser')
        if mathtran_parser is None:
            return self.formatter.text("Please install the mathtran parser!")

        ap = mathtran_parser(self.args, self.macro.request)
        if ap.init_settings:
            return ap.render(self.formatter)

def execute(macro, args):
    return mathtran(macro, args).renderInPage()
