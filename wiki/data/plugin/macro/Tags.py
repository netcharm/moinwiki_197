#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
     MoinMoin - macro to Readable pagename list

     @copyright: 2014 by netcharm
     @license: GNU GPL, see COPYING for details.
"""
import os
import sys

import codecs
import urllib

import string
import re


def macro_Tags(macro, tags, expand=False):
    fmt = macro.formatter
    #print(tags)

    # taglist = tags.split('.,;:-|/ ')
    taglist = []
    #print(re.findall(r"[\w']+", tags))
    taglist.extend(re.findall(r"[\w']+", tags))
    #print(re.findall(u"[\x80-\xff]+", tags))
    taglist.extend(re.findall(u"[\x80-\xff]+", tags.encode('utf8')))

    # print(len(taglist))

    result = ''
    # searchLink = '&value=%s&fullsearch=正文'
    # searchLink = '?action=fullsearch&advancedsearch=1&and_terms=%s'
    searchLink = u'?action=fullsearch&advancedsearch=1&and_terms=regex%%3A%%27%%3C%%3CTags\(.*%%3F%s.*%%3F\)%%3E%%3E%%27'
    #searchLink = u'?action=fullsearch&advancedsearch=1&and_terms=re%%3A<<Tags.*%%3F>>(%s)'

    if len(taglist)>0:
        tagLinks = []
        for tag in taglist:
            link = searchLink % tag
            #'<li>' +
            tagLink = '' +  \
                      fmt.url(True, href=link, title=tag, css_class=u"btn btn-default") + \
                      fmt.text(tag) + \
                      fmt.url(False) + \
                      '' #</li>'
            tagLinks.append(tagLink)
        #print(tagLink)

        if len(tagLinks) > 1:
            tagsClass = u"glyphicon glyphicon-tags"
        else:
            tagsClass = u"glyphicon glyphicon-tag"
        tagsContent = u'<div class="btn-group btn-group-xs">%s</div>' % '\n'.join(tagLinks)
        tagsAttr = u'data-delay="{ show: 0, hide: 200 }"'

        result = u'''
        <span class="%s" data-content='%s' data-placement="bottom" data-toggle="popover" data-html="true">
        </span>&nbsp;
        ''' % ( tagsClass, tagsContent)

        if expand:
            return(fmt.rawHTML(tagsContent))
        else:
            return(fmt.rawHTML(result))
            # return(result)

    return(result)



