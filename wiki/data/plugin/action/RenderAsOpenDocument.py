# -*- coding: utf-8 -*-

"""
    MoinMoin - OpenDocument Formatter - action class

    (c) 2007-2008 Hans-Peter Schaal <hp.news@gmx.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

from MoinMoin.Page import Page
from MoinMoin import wikiutil, config
import os, errno, tempfile, time, zipfile, codecs, shutil


# Helper Method: Returns (line, nextReadPosition) or ("", -1) if startpos is at end of rawString
def _readline( rawString, startpos ):
    CRLF = "\n"
    if startpos < 0 or startpos >= len( rawString ): return ("", -1)
    lineend = rawString.find( CRLF, startpos )
    if lineend == -1: return (rawString[ startpos : ], len(rawString) )
    return ( rawString[ startpos : lineend ], lineend + len(CRLF) )


# Helper method: Returns (attachments, pure content string) while
# attachments = [ ( filename1, internalfilename1, mimetype1 ), ... ]
def _filter_content( rawString ):
    nextpos = 0
    attachments = []
    filename, internal, mimetype = None, None, None
    
    while nextpos >= 0:
        # Is dataset completely read? Then write and go to next one
        if None not in (filename, internal, mimetype):
            attachments.append( (filename, internal, mimetype) )
            filename, internal, mimetype = None, None, None
        # Read next line
        (line, nextpos) = _readline( rawString, nextpos )
        if line.startswith("</pseudoformatter_header>"):
            break
        if line.startswith("ATTACHMENT:"):
            filename = line[ len("ATTACHMENT:") : ]
        if line.startswith("INTERNAL:"):
            internal = line[ len("INTERNAL:") : ]
        if line.startswith("MIMETYPE:"):
            mimetype = line[ len("MIMETYPE:") : ]
    
    return ( attachments, rawString[ nextpos : ] )
    

def execute(pagename, request):

    # MoinMoin standalone can't import a user plugin via import "MoinMoin.formatter.appXY..."
    # And since we're not in folder "formatter" we can't use "import appXY..."
    Formatter = wikiutil.importPlugin(request.cfg, 'formatter', 'application_odt_pseudoformatter', 'Formatter')

    # wikiutil.quoteWikinameURL produces ugly filenames. So we preprocess the filename.
    replacement = { u'\xe4' : u'ae',   u'\xfc' : u'ue',   u'\xf6' : u'oe',   u'\xdf' : u'ss', \
                                  u'\xc4' : u'Ae',   u'\xdc' : u'Ue',   u'\xd6' : u'Oe', \
                                  u' ' : u'_',   u'/' : u'_' }
    newPagename = pagename + '.odt'
    for key in replacement:
        newPagename = newPagename.replace( key, replacement[key] )
    filenameASCII = wikiutil.quoteWikinameURL( newPagename )

    # Prepare output directory
    outputdir = tempfile.mkdtemp()
    try:
        os.mkdir( outputdir )
    except OSError, err:
        if err.errno != errno.EEXIST:
            raise RuntimeError, "Cannot create output directory '%s'!" % ( outputdir )

    # create temporary ZIP file
    pathToZIP = os.path.join( outputdir, filenameASCII )
    zipper = zipfile.ZipFile( pathToZIP, "w" )

    # we need current time for ZipInfo objects
    now = time.localtime(time.time())[:6]
    
    # most files in container have to be put in manifest.xml
    innerfiles = []

    # write inner file "mimetype"
    info = zipfile.ZipInfo("mimetype", now)
    info.compress_type = zipfile.ZIP_STORED     # ZIP_STORED = uncompressed
    zipper.writestr(info, "application/vnd.oasis.opendocument.text")

    # write inner file content.xml by calling the util formatter (returns unicode which has to be encoded as utf-8)
    formatter = Formatter(request, store_pagelinks=1)
    page = Page(request, pagename, formatter = formatter)
    
    (attachments, contentString) = \
        _filter_content( request.redirectedOutput(page.send_page, emit_headers=0, count_hit=0).encode( 'utf-8' ) )
    
    info = zipfile.ZipInfo("content.xml", now)
    info.compress_type = zipfile.ZIP_DEFLATED     # ZIP_DEFLATED = compressed
    zipper.writestr(info, contentString )
    innerfiles.append(("text/xml","content.xml"))

    # include attachment files
    for (filename, internalname, mimetype) in attachments:
        zipper.write(filename, internalname, zipfile.ZIP_STORED)  # ZIP_STORED = uncompressed
        innerfiles.append( (mimetype, internalname) )

    # create inner file "styles.xml"
    stylesStr = """<?xml version="1.0" encoding="utf-8"?>
<office:document-styles xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
xmlns:xlink="http://www.w3.org/1999/xlink"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
xmlns:dom="http://www.w3.org/2001/xml-events" office:version="1.0">
  <office:font-face-decls>
    <style:font-face style:name="StarSymbol" svg:font-family="StarSymbol"/>
    <style:font-face style:name="Bitstream Vera Sans Mono"
    svg:font-family="'Bitstream Vera Sans Mono'"
    style:font-family-generic="modern" style:font-pitch="fixed" />
    <style:font-face style:name="Bitstream Vera Sans"
    svg:font-family="'Bitstream Vera Sans'"
    style:font-family-generic="roman"
    style:font-pitch="variable" />
    <style:font-face style:name="Bitstream Vera Sans1"
    svg:font-family="'Bitstream Vera Sans'"
    style:font-family-generic="swiss"
    style:font-pitch="variable" />
    <style:font-face style:name="Bitstream Vera Sans2"
    svg:font-family="'Bitstream Vera Sans'"
    style:font-family-generic="system"
    style:font-pitch="variable" />
  </office:font-face-decls>
  <office:styles>
    <style:default-style style:family="paragraph">
      <style:paragraph-properties fo:hyphenation-ladder-count="no-limit"
      style:text-autospace="ideograph-alpha"
      style:punctuation-wrap="hanging" style:line-break="strict"
      style:tab-stop-distance="1.251cm"
      style:writing-mode="page"  />
      <style:text-properties style:use-window-font-color="true"
      style:font-name="Bitstream Vera Sans" fo:font-size="11pt"
      fo:language="de" fo:country="DE" style:letter-kerning="true"
      style:font-name-asian="Bitstream Vera Sans2"
      style:font-size-asian="10.5pt" style:language-asian="zxx"
      style:country-asian="none"
      style:font-name-complex="Bitstream Vera Sans2"
      style:font-size-complex="11pt" style:language-complex="zxx"
      style:country-complex="none" fo:hyphenate="false"
      fo:hyphenation-remain-char-count="2"
      fo:hyphenation-push-char-count="2" />
    </style:default-style>
    <style:default-style style:family="table">
      <style:table-properties table:border-model="collapsing" />
    </style:default-style>
    <style:default-style style:family="table-row">
      <style:table-row-properties fo:keep-together="auto" />
    </style:default-style>
    <style:style style:name="Standard" style:family="paragraph"
    style:class="text">
      <style:paragraph-properties fo:margin-top="0.2cm"
      fo:margin-bottom="0.2cm" />
    </style:style>
    <style:style style:name="Numbering_20_Symbols" 
    style:display-name="Numbering Symbols" style:family="text"/>
    <style:style style:name="Text_20_body"
    style:display-name="Text body" style:family="paragraph"
    style:parent-style-name="Standard" style:class="text">
      <style:paragraph-properties fo:margin-top="0cm"
      fo:margin-bottom="0.212cm" />
    </style:style>
    <style:style style:name="Heading" style:family="paragraph"
    style:parent-style-name="Standard"
    style:next-style-name="Text_20_body" style:class="text">
      <style:paragraph-properties fo:margin-top="0.423cm"
      fo:margin-bottom="0.212cm" fo:keep-with-next="always" />
      <style:text-properties style:font-name="Bitstream Vera Sans1"
      fo:font-size="12pt"
      style:font-name-asian="Bitstream Vera Sans2"
      style:font-size-asian="12pt"
      style:font-name-complex="Bitstream Vera Sans2"
      style:font-size-complex="12pt" />
    </style:style>
    <style:style style:name="Heading_20_1"
    style:display-name="Heading 1" style:family="paragraph"
    style:parent-style-name="Heading"
    style:next-style-name="Text_20_body" style:class="text"
    style:default-outline-level="1">
      <style:text-properties fo:font-size="130%"
      fo:font-weight="bold" style:font-size-asian="130%"
      style:font-weight-asian="bold" style:font-size-complex="130%"
      style:font-weight-complex="bold" />
    </style:style>
    <style:style style:name="Heading_20_2"
    style:display-name="Heading 2" style:family="paragraph"
    style:parent-style-name="Heading"
    style:next-style-name="Text_20_body" style:class="text"
    style:default-outline-level="2">
      <style:text-properties fo:font-size="115%"
      fo:font-style="italic" fo:font-weight="bold"
      style:font-size-asian="115%" style:font-style-asian="italic"
      style:font-weight-asian="bold" style:font-size-complex="115%"
      style:font-style-complex="italic"
      style:font-weight-complex="bold" />
    </style:style>
    <style:style style:name="Heading_20_3"
    style:display-name="Heading 3" style:family="paragraph"
    style:parent-style-name="Heading"
    style:next-style-name="Text_20_body" style:class="text"
    style:default-outline-level="3">
      <style:text-properties fo:font-size="12pt"
      fo:font-weight="bold" style:font-size-asian="12pt"
      style:font-weight-asian="bold" style:font-size-complex="12pt"
      style:font-weight-complex="bold" />
    </style:style>
    <style:style style:name="Heading_20_4"
    style:display-name="Heading 4" style:family="paragraph"
    style:parent-style-name="Heading"
    style:next-style-name="Text_20_body" style:class="text"
    style:default-outline-level="4">
      <style:text-properties fo:font-size="85%"
      fo:font-style="italic" fo:font-weight="bold"
      style:font-size-asian="85%" style:font-style-asian="italic"
      style:font-weight-asian="bold" style:font-size-complex="85%"
      style:font-style-complex="italic"
      style:font-weight-complex="bold" />
    </style:style>
    <style:style style:name="List" style:family="paragraph"
    style:parent-style-name="Text_20_body" style:class="list">
      <style:text-properties style:font-size-asian="11pt" />
    </style:style>
    <style:style style:name="Caption" style:family="paragraph"
    style:parent-style-name="Standard" style:class="extra">
      <style:paragraph-properties fo:margin-top="0.212cm"
      fo:margin-bottom="0.212cm" text:number-lines="false"
      text:line-number="0" />
      <style:text-properties fo:font-size="11pt"
      fo:font-style="italic" style:font-size-asian="11pt"
      style:font-style-asian="italic"
      style:font-size-complex="11pt"
      style:font-style-complex="italic" />
    </style:style>
    <style:style style:name="Index" style:family="paragraph"
    style:parent-style-name="Standard" style:class="index">
      <style:paragraph-properties text:number-lines="false"
      text:line-number="0" />
      <style:text-properties style:font-size-asian="11pt" />
    </style:style>
    <style:style style:name="MoinDefinitionTerm"
    style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-top="0.2cm" />
      <style:text-properties style:font-name="Bitstream Vera Sans Mono"
      fo:font-weight="bold"
      style:font-weight-asian="bold"
      style:font-weight-complex="bold" />
    </style:style>
    <style:style style:name="Preformatted_20_Text"
    style:display-name="Preformatted Text" style:family="paragraph"
    style:parent-style-name="Standard" style:class="html"
    style:master-page-name="">
      <style:paragraph-properties fo:margin-left="0cm"
      fo:margin-right="0cm" fo:margin-top="0cm"
      fo:margin-bottom="0cm" fo:line-height="125%"
      style:register-true="true" fo:text-indent="0cm"
      style:auto-text-indent="false" fo:background-color="#f2f2f2"
      fo:padding="0.199cm" fo:border="0.002cm solid #4c4c4c"
      style:shadow="none" />
      <style:text-properties style:font-name="Bitstream Vera Sans Mono"
      fo:font-size="10pt"
      style:font-name-asian="Bitstream Vera Sans Mono"
      style:font-size-asian="10pt"
      style:font-name-complex="Bitstream Vera Sans Mono"
      style:font-size-complex="10pt" />
    </style:style>
    <style:style style:name="Horizontal_20_Line" 
    style:display-name="Horizontal Line" style:family="paragraph" 
    style:parent-style-name="Standard" 
    style:next-style-name="Text_20_body" style:class="html">
      <style:paragraph-properties fo:margin-top="0cm" 
      fo:margin-bottom="0.5cm" 
      fo:padding="0cm" fo:border-left="none" fo:border-right="none" 
      fo:border-top="none" 
      fo:border-bottom="0.02cm solid #606060" 
      text:number-lines="false" text:line-number="0"/>
      <style:text-properties fo:font-size="6pt" style:font-size-asian="6pt" 
      style:font-size-complex="6pt"/>
    </style:style>
    <style:style style:name="Bullet_20_Symbols" 
    style:display-name="Bullet Symbols" style:family="text">
      <style:text-properties style:font-name="StarSymbol" 
      fo:font-size="9pt" style:font-name-asian="StarSymbol" 
      style:font-size-asian="9pt" style:font-name-complex="StarSymbol" 
      style:font-size-complex="9pt"/>
    </style:style>
    <text:list-style style:name="MoinNumberedList">
    <text:list-level-style-number text:level="1"
    text:style-name="Numbering_20_Symbols" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="0.6cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="2" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="1.2cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="3" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="1.8cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="4" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="2.4cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="5" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="3.0cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="6" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="3.6cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="7" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="4.2cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="8" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="4.8cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="9" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="5.4cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="10" style:num-suffix="."
    style:num-format="1">
    <style:list-level-properties text:space-before="6.0cm"
    text:min-label-width="0.6cm" />
    </text:list-level-style-number>
    </text:list-style>
    <text:list-style style:name="MoinHiddenList">
    <text:list-level-style-number text:level="1"
    text:style-name="Numbering_20_Symbols" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="1.2cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="2" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="1.8cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="3" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="2.4cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="4" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="3.0cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="5" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="3.6cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="6" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="4.2cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="7" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="4.8cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="8" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="5.4cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="9" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="6.0cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    <text:list-level-style-number text:level="10" style:num-suffix=""
    style:num-format="">
    <style:list-level-properties text:space-before="6.6cm"
    text:min-label-width="0.0cm" />
    </text:list-level-style-number>
    </text:list-style>
    <text:list-style style:name="MoinBulletList">
      <text:list-level-style-bullet text:level="1" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="●">
        <style:list-level-properties text:space-before="0.6cm" 
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="2" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="○">
        <style:list-level-properties text:space-before="1.2cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="3" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="■">
        <style:list-level-properties text:space-before="1.8cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="4" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="●">
        <style:list-level-properties text:space-before="2.4cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="5"
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="○">
        <style:list-level-properties text:space-before="3.0cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="6" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="■">
        <style:list-level-properties text:space-before="3.6cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="7" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="●">
        <style:list-level-properties text:space-before="4.2cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="8" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="○">
        <style:list-level-properties text:space-before="4.8cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="9" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="■">
        <style:list-level-properties text:space-before="5.4cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
      <text:list-level-style-bullet text:level="10" 
      text:style-name="Bullet_20_Symbols" style:num-suffix="." 
      text:bullet-char="●">
        <style:list-level-properties text:space-before="6.0cm"
        text:min-label-width="0.6cm"/>
        <style:text-properties style:font-name="StarSymbol"/>
      </text:list-level-style-bullet>
    </text:list-style>  
    <text:outline-style>
      <text:outline-level-style text:level="1" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="2" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="3" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="4" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="5" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="6" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="7" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="8" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="9" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
      <text:outline-level-style text:level="10" style:num-format="">
        <style:list-level-properties text:min-label-distance="0.4cm" />
      </text:outline-level-style>
    </text:outline-style>
    <text:notes-configuration text:note-class="footnote"
    style:num-format="1" text:start-value="0"
    text:footnotes-position="page"
    text:start-numbering-at="document" />
    <text:notes-configuration text:note-class="endnote"
    style:num-format="i" text:start-value="0" />
    <text:linenumbering-configuration text:number-lines="false"
    text:offset="0.499cm" style:num-format="1"
    text:number-position="left" text:increment="5" />
  </office:styles>
  <office:automatic-styles>
    <style:page-layout style:name="pm1">
      <style:page-layout-properties fo:page-width="20.999cm"
      fo:page-height="29.699cm" style:num-format="1"
      style:print-orientation="portrait" fo:margin-top="2cm"
      fo:margin-bottom="2cm" fo:margin-left="2cm"
      fo:margin-right="2cm" style:writing-mode="lr-tb"
      style:footnote-max-height="0cm">
        <style:footnote-sep style:width="0.018cm"
        style:distance-before-sep="0.101cm"
        style:distance-after-sep="0.101cm" style:adjustment="left"
        style:rel-width="25%" style:color="#000000" />
      </style:page-layout-properties>
      <style:header-style />
      <style:footer-style />
    </style:page-layout>
  </office:automatic-styles>
  <office:master-styles>
    <style:master-page style:name="Standard"
    style:page-layout-name="pm1" />
  </office:master-styles>
</office:document-styles>"""

    # write inner file styles.xml
    info = zipfile.ZipInfo("styles.xml", now)
    info.compress_type = zipfile.ZIP_DEFLATED  # ZIP_DEFLATED = compressed
    zipper.writestr(info, stylesStr)
    innerfiles.append(("text/xml","styles.xml"))

    # write inner file meta.xml
    metaStr = """<?xml version="1.0" encoding="UTF-8"?>
<office:document-meta xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
xmlns:xlink="http://www.w3.org/1999/xlink">
  <office:meta>
    <meta:generator>MoinMoin OpenDocument Formatter</meta:generator>
  </office:meta>
</office:document-meta>"""

    info = zipfile.ZipInfo("meta.xml", now)
    info.compress_type = zipfile.ZIP_STORED  # ZIP_STORED = uncompressed
    zipper.writestr(info, metaStr)
    innerfiles.append(("text/xml","meta.xml"))

    # write inner file settings.xml
    settingsStr = """<?xml version="1.0" encoding="UTF-8"?>
<office:document-settings xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
xmlns:xlink="http://www.w3.org/1999/xlink"/>"""

    info = zipfile.ZipInfo("settings.xml", now)
    info.compress_type = zipfile.ZIP_STORED  # ZIP_STORED = uncompressed
    zipper.writestr(info, settingsStr)
    innerfiles.append(("text/xml","settings.xml"))

    # last write inner file META-INF/manifest.xml
    manifestStr ="""<?xml version="1.0" encoding="UTF-8"?>
<manifest:manifest xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0">
  <manifest:file-entry manifest:full-path="/" manifest:media-type="application/vnd.oasis.opendocument.text"/>
"""
    for ( mediatype, fullpath ) in innerfiles:
        manifestStr = manifestStr + '  <manifest:file-entry manifest:full-path="' + fullpath + \
                                '" manifest:media-type="' + mediatype + '"/>\n'

    manifestStr = manifestStr + '</manifest:manifest>'
    info = zipfile.ZipInfo("META-INF/manifest.xml", now)
    info.compress_type = zipfile.ZIP_STORED  # ZIP_STORED = uncompressed
    zipper.writestr(info, manifestStr )

    # close ZIP file
    zipper.close()


    # Set HTTP Header: mimetype and filename
    #
    # Note that RuntimeErrors create their one headers and pages and thus should come before this: Now's too late
    # for HTML-Error pages.
    request.headers.add('Content-Type','application/vnd.oasis.opendocument.text')
    request.headers.add('Content-Disposition: attachment','filename="%s"'  % (filenameASCII))

    # send data (without 'rb' the windows version doesn't work at all!)
    shutil.copyfileobj( open( pathToZIP, 'rb' ), request, 8192 )

    # remove temporary data
    shutil.rmtree(outputdir)
