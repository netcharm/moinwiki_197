#!/usr/bin/env python
# -*- coding: utf-8 -*-


import re

import urllib2
import urlparse

import json

from MoinMoin import wikiutil
from MoinMoin.parser.text_moin_wiki import Parser as WikiParser
from MoinMoin.Page import Page


def GetNoticeItem(request, pagename):

    if request.user.may.read(pagename):
        page = Page(request, pagename)
        page_raw = page.get_raw_body()
    else:
        page_raw =u''

    notice_items = re.findall(r'(\|\|.+\|\|.+\|\|.+\|\|.+\|\|)', page_raw)

    notices = []
    count = 0
    if len(notice_items)>2:
        notice_items = notice_items[1:]

    for item in notice_items:
        item_data = item.split(u'||')

        notice_id = count
        notice_st = item_data[1].strip()
        notice_et = item_data[2].strip()
        notice_tt = item_data[3].strip()
        notice_tx = item_data[4].strip()
        notice_sl = item_data[5].strip()

        # This action generate data using the user language

        page = Page(request, "")
        request.formatter.setPage(page)
        notice_tx = wikiutil.renderText(request, WikiParser, notice_tx).strip()

        title = re.findall(r'<h\d+.*</a>(.*?)</h\d+>', notice_tx)
        if len(title)>0:
            title = title[0]
        else:
            title = u''

        TAG_RE = re.compile(r'<[^>]+>')
        notice_tx = TAG_RE.sub('', notice_tx).encode('utf8')

        # print(u'===================')
        # print(u'title    : %s' % title)
        # print(u'-------------------')
        if len(notice_tt) == 0:
            notice_tt = title

        # print(u'notice_tt: %s' % notice_tt)
        # print(u'-------------------')

        # notice_tx = re.sub(r'<h3.*?</h3>', '', notice_tx)
        notice_tx = notice_tx.replace(title.encode('utf8'), '')

        # print(u'notice_tx: %s' % notice_tx.decode('utf8'))
        # print(u'===================')

        notice = dict()
        notice['id']    = notice_id
        notice['start'] = notice_st
        notice['stop']  = notice_et
        notice['title'] = notice_tt
        notice['text']  = notice_tx
        notice['style'] = notice_sl

        notices.append(notice)

    return(notices)


def execute(pagename, request):
    _ = request.getText

    content = u'None'

    mode = wikiutil.escape(request.form.get('mode', ['None']))
    page = wikiutil.escape(request.form.get('page', ['None']))

    if page == u"['None']":
        # page = _(u'TimeNotice')
        page = u'TimeNotice'

    notices = GetNoticeItem(request, page)

    if mode.lower() == u'json':
        content = json.dumps(notices)
        request.write(content)
    else:
        content = u'''
{{{
%s
}}}
        ''' % json.dumps(notices)

        link = WikiParser(content, request)


        request.write(link)

