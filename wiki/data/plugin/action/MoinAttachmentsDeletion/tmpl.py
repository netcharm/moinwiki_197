'''
tmpl.py -- templating

----
Copyright (C) 2007-2008 Zoran Isailovski
Licensed under the Apache License, Version 2.0 (see http://www.apache.org/licenses/LICENSE-2.0)
or, at your option, under the terms described in the file LICENSE.txt found in the root of the 
distribution of this package (as far as it denotes a different license then the above).
'''
import os
import airspeed as velocity

TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), '')
TEMPLATE_SFX = '.vt.html'
TEMPLATE_PFX = 'tmpl.'

loader = velocity.CachingFileLoader(TEMPLATE_DIR)
new = velocity.Template

def renderThis(templateName, **data):
    return render(this(templateName), **data)

def render(template, **data):
    return template.merge(data, loader)

def this(templateName):
    templateName = fileNameOf(templateName)
    return loader.load_template(templateName)

def fileNameOf(templateName):
    s0 = templateName
##    if not os.path.splitext(templateName)[1]:
##        templateName += TEMPLATE_SFX

    t = templateName.lower()
    path = TEMPLATE_DIR + templateName
    if not os.path.isfile(path) and not t.endswith(TEMPLATE_SFX):
        templateName += TEMPLATE_SFX
        path = TEMPLATE_DIR + templateName
    if not os.path.isfile(path) and not t.startswith(TEMPLATE_PFX):
        templateName = TEMPLATE_PFX + templateName
        path = TEMPLATE_DIR + templateName
    assert os.path.isfile(path), 'Template not found: ' + s0
    return templateName

