﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
     MoinMoin - Action to get a storage folder name for this page

     @copyright: 2014 by netcharm
     @license: GNU GPL, see COPYING for details.
"""

from MoinMoin import wikiutil

def execute(pagename, request):
    _ = request.getText

    mode = wikiutil.escape(request.form.get('mode', ['None']))
    if mode.lower() == 'json':
        pass
    elif mode.lower() == 'raw':
        request.write(wikiutil.quoteWikinameFS(pagename))
        # request.write(PageToFolder(pagename.encode('utf8')))
    else:
        request.setContentLanguage(request.lang)

        request.theme.send_title(_('Page %s folder name readable') % (pagename), pagename=pagename)

        # Start content - IMPORTANT - without content div, there is no
        # direction support!
        request.write(request.formatter.startContent("content"))

        request.write(wikiutil.quoteWikinameFS(pagename))

        # End content and send footer
        request.write(request.formatter.endContent())
        request.theme.send_footer(pagename)
        request.theme.send_closing_html()
