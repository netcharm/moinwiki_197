# -*- coding: utf-8 -*-
"""
    MoinMoin - CreateNewPage Action

    XXX ToDo
    Provide translations for:
    _('Page Template')
    _('Page Name')
    _('Create')

    CreateNewPage partly based on NewPage action
    NewPage Action
    @copyright: 2004 Vito Miliano (vito_moinnewpagewithtemplate@perilith.com)
    @copyright: 2004 by Nir Soffer <nirs@freeshell.org>
    @copyright: 2004 Alexander Schremmer <alex AT alexanderweb DOT de>

    CreateNewPage Action
    @copyright: 2007 by Oliver Siemoneit
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin import wikiutil
from MoinMoin.Page import Page

def execute(pagename, request):
    return NewPageHandler(pagename, request).handle()

class NewPageHandler:

    def __init__(self, pagename, request):
        self.request = request
        self._ = request.getText
        self.pagename = pagename
        self.newpage = ''
        self.template = ''

    def showForm(self, msg=None):
        """ Assemble form and output it

        @rtype: -
        @return: - 
        """
        _ = self._
        request = self.request

        # Get available templates
        templates = []
        filter = request.cfg.cache.page_template_regex.search 
        results = request.rootpage.getPageList(filter=filter)
        results.sort()
        templates.append("<option selected=\"selected\">%s</option>" % '') # _('<None>')
        for result in results:
            templates.append("<option>%s</option>" % result)

        # Assemble form
        sn = request.script_root
        pi = request.path
        action = u"%s%s" % (sn, pi)
        lang_attr = request.theme.ui_lang_attr()

        form = '''
            <form action="%(action)s" method="POST">
            <div class="userpref" lang="%(lang_attr)s">
            <input type="hidden" name="action" value="CreateNewPage" />
            <table style="border:0;">
            <tr>
            <td><span class="bold">%(template)s</span></td>
            <td> 
            <select name="template" size="1">
            %(templates)s
            </select>
            </td>
            </tr>
            <tr>
            <td><span class="bold">%(input)s</span></td>
            <td><input type="text" name="newpage" value="" size="36" /></td>
            </tr>
            <tr>
            <td>&#160;</td>
            <td><input type="submit" name="create" value="%(button)s" /></td>
            </tr>
            </table>
            </div>
            </form>''' % { 'action': action,
           'lang_attr': lang_attr,
           'template': _('Page Template'),
           'templates': ''.join(templates),
           'input': _('Page Name'),
           'button': _('Create') }
        
        # Output form
        # fixed for 1.9
        try:
            request.emit_http_headers()
        except AttributeError:
            try:
                request.http_headers()
            except AttributeError:
                pass
        request.theme.send_title(_('Create New Page'), pagename=self.pagename, msg=msg)
        request.write(request.formatter.startContent("content"))
        request.write(request.formatter.rawHTML(unicode(form)))
        request.write(request.formatter.endContent())
        request.theme.send_footer(self.pagename)
        request.theme.send_closing_html()

    def checkInput(self):
        """ Check if input is valid 

        @rtype: unicode
        @return: error message
        """
        _ = self._
        self.newpage = wikiutil.escape(self.newpage.strip())
        self.template = wikiutil.escape(self.template.strip())
        if not self.newpage:
            return _("Cannot create a new page without a page name."
                     "  Please specify a page name.")
        page = Page(self.request, self.newpage)
        if not (page.isWritable() and self.request.user.may.read(self.newpage)):
            return _('You are not allowed to edit this page.')
        return ''

    def handle(self):
        request = self.request
        form = self.request.form
        # If user has pressed "create" button, process it. Otherwise show form only.
        if form.has_key('create'):
            self.newpage = form.get('newpage', [''])[0]
            self.template = form.get('template', [''])[0]
            error = self.checkInput()
            if error:
                self.showForm(error)
            else:
                query = {'action': 'edit', 'backto': self.pagename}
                if self.template:
                    query['template'] = wikiutil.quoteWikinameURL(self.template)
                url = Page(request, self.newpage).url(request, query, escape=0, relative=False)
                self.request.http_redirect(url)
        else:
            self.showForm()            
        return ''

            

