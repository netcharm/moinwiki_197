# -*- coding: utf-8 -*-

import os
import sys

import codecs
import urllib
import re


def split_len(seq, length):
    return [seq[i:i+length] for i in range(0, len(seq), length)]

def raw2str(matchobj):
    raw_str = matchobj.group(0)
    quote_str = '%' + '%'.join(split_len(raw_str.strip('()'),2))
    result = u"%s" % urllib.unquote(quote_str)

    return result

def getfoldername(fn_encoded):
    #mrs = re.findall('(\(.*?.\))', fn_encoded)

    fn_decoded = re.sub('(\(.*?.\))', raw2str, fn_encoded)
    #print fn_decoded
    return(fn_decoded)

def getfoldernames():
    CWD = os.getcwd()
    # result = os.path.walk(CWD, get_foldername)
    dirs_encoded = os.listdir(CWD)
    dirs_decoded = map(getfoldername, dirs_encoded)

    #print(dirs_decoded, dirs_encoded)
    return(dirs_decoded, dirs_encoded)

def FolderToPage(folder):
    return(getfoldername(folder))

def str2raw(matchobj):
    quote_str = matchobj.group(0)
    quote_str = quote_str.replace('%', '')
    result = u"(%s)" % urllib.unquote(quote_str)

    return result.lower()

def PageToFolder(page):
    quote_str = urllib.quote(page)

    fn_encoded = re.sub('(\%.{2})+', str2raw, quote_str)

    print(fn_encoded.replace('/', '(2f)'))


def save2text(csv, BOM=True):
    lines = [('%s, %s\n'% (a[0], a[1])) for a in zip(csv[0], csv[1])]
    print(lines)

    outfile = 'pagenames.txt'
    f = codecs.open(outfile, 'w', 'utf8')

    if BOM:
      f.write(codecs.BOM_UTF8)

    f.writelines(lines)
    f.close()


if __name__ == '__main__':
    save2text(getfoldernames())

    # 'netcharm(2f)WEB(e9a1b5e99da2e8bdace68da2)'
    # 'netcharm/WEB页面转换'
    # PageToFolder('netcharm/WEB页面转换')
    # '(e7bb99)MoinMoin(e58699e68f92e4bbb6)'
    # '给MoinMoin写插件'
    # PageToFolder('给MoinMoin写插件')

