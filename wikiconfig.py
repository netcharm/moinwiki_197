#!/usr/bin/env python
# -*- coding: utf-8 -*-

"MoinMoin - Configuration"
import os
import sys

from MoinMoin.config.multiconfig import DefaultConfig

class Config(DefaultConfig):
    CWD = os.path.abspath(os.path.join(os.path.dirname(__file__)))
    WIKIROOT = os.path.join(CWD, 'wiki')
    print(u'wiki folder   : %s' % WIKIROOT)

    # docs = os.path.join(WIKIROOT, 'wiki')
    port = 9000

    sitename = u'Home Moin'
    interwikiname = u'Home Moin'


    # acl_rights_after = acl_rights_default = u"All:read,write,delete,revert,admin"
    acl_rights_after = acl_rights_default = u"Known:read,write,delete,revert,admin All:read"

    cookie_lifetime = (0, 9000)

    # instance_dir = WIKIROOT

    # url_prefix = '/moin_static197' #模板文件的设置，如果主题样式显示有问题，就是这里设置

    superuser = [u"netcharm", u"lily",]
    surge_action_limits = None # disable surge protection

    edit_on_doubleclick = False

    theme_default = 'bootstrap'
    theme_force = True

    language_default = 'zh'
    language_ignore_browser = True

    page_front_page = 'RecentChange'

    data_dir = os.path.join(WIKIROOT, 'data')
    underlay_dir = os.path.join(WIKIROOT, 'underlay')
    data_underlay_dir = os.path.join(WIKIROOT, 'underlay')

    span_supports_style = True

    page_category_regex = ur'(?P<all>(?P<prefix>[Category|tag])?(?P<key>\S+)(?(prefix)|类))'
    page_dict_regex = ur'(?P<all>(?P<key>\S+)(Dict|字典))'
    page_group_regex = ur'(?P<all>(?P<key>\S+)(Group|组))'
    page_template_regex = ur'(?P<all>(?P<key>\S+)(Template|模板))'

    # xapian_index_dir =
    # xapian_search = True
    xapian_stemming = True
    xapian_index_history = True