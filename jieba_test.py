#encoding=utf-8
import os
import sys

import codecs

import jieba

def jieba_demo(text=None):
  print(u'==== Cut All ====')
  if text:
    seg_list = jieba.cut(text, cut_all=True)
  else:
    seg_list = jieba.cut("我来到北京清华大学", cut_all=True)

  print " / ".join(seg_list)  # 全模式

  print(u'==== Cut ====')
  if text:
    seg_list = jieba.cut(text, cut_all=False)
  else:
    seg_list = jieba.cut("我来到北京清华大学", cut_all=False)
  print " / ".join(seg_list)  # 精确模式

  print(u'==== Cut default ====')
  if text:
    seg_list = jieba.cut(text)
  else:
    seg_list = jieba.cut("他来到了网易杭研大厦")  # 默认是精确模式
  print " / ".join(seg_list)

  print(u'==== Cut Search ====') # 搜索引擎模式
  if text:
    seg_list = jieba.cut_for_search(text)
  else:
    seg_list = jieba.cut_for_search("小明硕士毕业于中国科学院计算所，后在日本京都大学深造")  # 搜索引擎模式
  print " / ".join(seg_list)

  for seg in seg_list:
    print seg

#  print(u'==== Cut Index ====')
#  if text:
#    seg_list = jieba.cut_for_index(text)
#  else:
#    seg_list = jieba.cut_for_index("小明硕士毕业于中国科学院计算所，后在日本京都大学深造")  # 搜索引擎模式
#  print ", ".join(seg_list)

def scseg_demo(text=None):
  import scseg
  seg_list = scseg.seg_keywords(text)
  print '\n'.join(seg_list)

if __name__ == '__main__':
  text = u'衣带渐宽终不悔，为伊消得人憔悴。'

  jieba.load_userdict('poem.dic')
  jieba.initialize()
  jieba_demo(text)



